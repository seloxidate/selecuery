use crate::visitor::conditional_expression_context::{
    ConditionalExpressionContextResolveStrategy, ConditionalExpressionContextVisitor,
    ConditionalExpressionDataLevel, ConditionalExpressionStrFmtContext,
};
use dracaena::ast::common::uppercase_first_letter;
use dracaena::ast::constant::Constant;
use dracaena::ast::intrinsics::Intrinsics;
use dracaena::ast::local_body::LocalBody;
use dracaena::ast::operator::*;
use dracaena::ast::parm::ParmElem;
use dracaena::ast::{
    assignment::*, column::*, declaration::*, eval::*, eval_name::*, expression::*, factor::*,
    field::*, field_column::*, find::*, from::*, function::*, index_hint::*, join::*,
    order_elem::*, order_group::*, qualifier::*, select_option::*, statement::*, table::*, term::*,
};
use dracaena::visitor::visitable::VisitorEnterLeave;
use dracaena::visitor::visitable::{Visitable, Visitor};
use std::collections::HashMap;
use std::collections::HashSet;
use std::iter;
use std::mem;

#[derive(Default)]
pub struct TranspilerVisitor<'a> {
    local_body_query_ast: Option<LocalBody>,
    table_name_in_select: &'a str,
    table_names_in_joins: Vec<&'a str>,
    table_names_from_root_to_children: Vec<&'a str>,
    select_first_only: HashMap<&'a str, &'a FirstOnly>,
    is_while_select: bool,
    current_join_table: &'a str,
    cond_expr_str_fmt_ctx: ConditionalExpressionStrFmtContext,
    cond_expr_add_range_stmts: Vec<Statement>,
    current_cond_expr_data_level: ConditionalExpressionDataLevel,
    join_statements: JoinStatements<'a>,
    join_link_fields_ctx: JoinLinkFieldsContext<'a>,
    join_cond_expr_ctx: JoinConditionalExpressionContext<'a>,
    cond_expr_ctx_resolve_strategy: Option<ConditionalExpressionContextResolveStrategy>,
    num_of_expressions_in_where_in_current_join_ctx: usize,
}

impl<'a> TranspilerVisitor<'a> {
    pub fn new() -> Self {
        Self {
            ..Default::default()
        }
    }

    pub fn into_transpiled_ast(self) -> Option<LocalBody> {
        let has_select_first_only_one = self.has_select_first_only_one();
        if let Some(mut local_body) = self.local_body_query_ast {
            // we append declarations for `query` and `queryRun`
            local_body.dcl_list.extend(vec![
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("Query".to_owned()),
                            id: "query".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryRun".to_owned()),
                            id: "queryRun".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
            ]);
            let table_names_in_decl = iter::once(self.table_name_in_select)
                .chain(self.table_names_in_joins.clone())
                .map(|table_var| DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId(uppercase_first_letter(table_var)),
                            id: table_var.to_owned(),
                        },
                        assignment_clause: None,
                    }),
                });
            local_body.dcl_list.extend(table_names_in_decl);

            local_body.stmt_list = iter::once(
                AssignmentStmt::FieldAssign {
                    field: Field::Id("query".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::New("Query".to_owned()),
                        parm_list: None,
                    })
                    .builder()
                    .into(),
                }
                .into(),
            )
            .chain(local_body.stmt_list)
            .chain(iter::once(
                AssignmentStmt::FieldAssign {
                    field: Field::Id("queryRun".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::New("QueryRun".to_owned()),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::Id("query".to_owned()))
                                .builder()
                                .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
            ))
            .collect();

            if has_select_first_only_one {
                local_body.stmt_list.push(
                    Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "next".to_owned(),
                        ),
                        parm_list: None,
                    }
                    .into(),
                );
            }
            if !self.is_while_select && has_select_first_only_one {
                local_body.stmt_list.extend(
                    iter::once(self.table_name_in_select)
                        .chain(self.table_names_in_joins)
                        .map(|table_name| {
                            AssignmentStmt::FieldAssign {
                                assign: Assign::AssignEqual,
                                field: Field::Id(table_name.into()),
                                tern_expr: Factor::Eval(Eval {
                                    eval_name: EvalName::Qualifier(
                                        Qualifier::Id("queryRun".to_owned()),
                                        "get".to_owned(),
                                    ),
                                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                                        Factor::Field(Field::QualifierId(
                                            Qualifier::Id(table_name.into()),
                                            "TableId".to_owned(),
                                        ))
                                        .builder()
                                        .into(),
                                    ))]),
                                })
                                .builder()
                                .into(),
                            }
                            .into()
                        }),
                )
            }
            Some(local_body)
        } else {
            None
        }
    }

    fn has_select_first_only_one(&self) -> bool {
        self.select_first_only
            .values()
            .any(|fo| **fo == FirstOnly::FirstOnly || **fo == FirstOnly::FirstOnly1)
    }

    /// we have to mutably borrow, because visit methods also borrow mutably
    fn is_in_join_ctx(&mut self) -> bool {
        !self.current_join_table.is_empty()
    }

    fn are_expressions_in_join_where_all_join_links(&mut self) -> bool {
        self.is_in_join_ctx()
            && self.num_of_expressions_in_where_in_current_join_ctx
                == self
                    .join_link_fields_ctx
                    .num_of_join_links(self.current_join_table)
    }
}

impl<'a> VisitorEnterLeave<'a> for TranspilerVisitor<'a> {
    fn leave_visit_find_join(&mut self, node: &'a FindJoin) -> bool {
        let local_body = self.local_body_query_ast.get_or_insert(Default::default());
        if let Some(join_list) = node.join_list.as_ref() {
            for join_spec in join_list {
                let join_table_name = join_spec.table_name();
                local_body
                    .stmt_list
                    .extend(self.join_statements.inner_into_statements(join_table_name));
                if self
                    .join_link_fields_ctx
                    .table_is_joined_sibling(join_table_name)
                {
                    local_body.stmt_list.push(
                        Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id(format!(
                                    "qbds{}",
                                    uppercase_first_letter(join_spec.table_name())
                                )),
                                "fetchMode".into(),
                            ),
                            parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                                Factor::Constant(Constant::Enum(
                                    "QueryFetchMode".into(),
                                    "One2One".into(),
                                ))
                                .builder()
                                .into(),
                            ))]),
                        }
                        .into(),
                    )
                }
            }
        }

        true
    }

    fn leave_visit_join_spec(&mut self, _node: &'a JoinSpec) -> bool {
        let are_expressions_in_join_where_all_join_links =
            self.are_expressions_in_join_where_all_join_links();

        self.join_statements.insert_many(
            self.current_join_table,
            self.join_link_fields_ctx.inner_into_statements(
                self.current_join_table,
                &self.table_names_from_root_to_children,
            ),
        );
        if !are_expressions_in_join_where_all_join_links {
            if let Some(cond_expr_ctx_resolve_strategy) = self.cond_expr_ctx_resolve_strategy.take()
            {
                self.join_statements.insert_many(
                    self.current_join_table,
                    self.join_cond_expr_ctx.inner_into_statements(
                        self.current_join_table,
                        cond_expr_ctx_resolve_strategy,
                    ),
                );
            }
        }

        self.num_of_expressions_in_where_in_current_join_ctx = 0;

        true
    }

    fn leave_visit_join_clause(&mut self, node: &'a JoinClause) -> bool {
        let local_body = self.local_body_query_ast.get_or_insert(Default::default());

        local_body.dcl_list.push(DeclarationStmt {
            decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                declaration: Declaration {
                    decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                    id: format!("qbds{}", uppercase_first_letter(node.table_name())),
                },
                assignment_clause: None,
            }),
        });

        true
    }

    fn leave_visit_find_where(&mut self, node: &'a FindWhere) -> bool {
        let table_name_first_letter_uppercased = uppercase_first_letter(node.table_name());
        let local_body = self.local_body_query_ast.get_or_insert(Default::default());

        let mut cond_expr_ctx_visitor = ConditionalExpressionContextVisitor::new();
        node.take_visitor_enter_leave(&mut cond_expr_ctx_visitor);
        let cond_expr_ctx_resolve_strategy = cond_expr_ctx_visitor.cond_expr_ctx_resolve_strategy;

        match cond_expr_ctx_resolve_strategy {
            None => {
                // TODO: We probably just need to append AddRange statements here to local body
            }
            Some(ConditionalExpressionContextResolveStrategy::AddRange) => {
                local_body
                    .stmt_list
                    .append(&mut self.cond_expr_add_range_stmts);
            }
            Some(ConditionalExpressionContextResolveStrategy::StrFmt) => {
                let cond_expr_ctx_for_range_value = mem::take(&mut self.cond_expr_str_fmt_ctx);
                local_body.stmt_list.push(
                    Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Eval(Box::new(Eval {
                                eval_name: EvalName::Qualifier(
                                    Qualifier::Id(format!(
                                        "qbds{}",
                                        table_name_first_letter_uppercased
                                    )),
                                    "addRange".to_owned(),
                                ),
                                parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                                    Factor::Intrinsics(
                                        // this range will have no effect in the end;
                                        // we use DataAreaId here as a convention indicating to the user
                                        // that the range will actually be useles, because it will be
                                        // solely determined by the query range **value**
                                        // see also: https://web.archive.org/web/20190305223625/http://www.axaptapedia.com/Expressions_in_query_ranges
                                        Intrinsics::FieldNum(
                                            table_name_first_letter_uppercased,
                                            "DataAreaId".to_owned(),
                                        ),
                                    )
                                    .builder()
                                    .into(),
                                ))]),
                            })),
                            "value".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Function(cond_expr_ctx_for_range_value.into_function_str_fmt())
                                .builder()
                                .into(),
                        ))]),
                    }
                    .into(),
                );
            }
        }

        true
    }

    fn leave_visit_find_table(&mut self, node: &'a FindTable) -> bool {
        let table_name_first_letter_uppercased = uppercase_first_letter(node.table_name());
        let mut statement_list = Vec::new();
        statement_list.push(
            AssignmentStmt::FieldAssign {
                assign: Assign::AssignEqual,
                field: Field::Id(format!("qbds{}", table_name_first_letter_uppercased)),
                tern_expr: Factor::Eval(Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("query".to_owned()),
                        "addDataSource".to_owned(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Field(Field::QualifierId(
                            Qualifier::Id(node.table_name().to_owned()),
                            "TableId".to_owned(),
                        ))
                        .builder()
                        .into(),
                    ))]),
                })
                .builder()
                .into(),
            }
            .into(),
        );
        if let Some(_fo) = self.select_first_only.get(node.table_name()) {
            statement_list.push(
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id(format!("qbds{}", table_name_first_letter_uppercased)),
                        "firstOnly".to_owned(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::True).builder().into(),
                    ))]),
                }
                .into(),
            )
        }
        if let Some(statement_list_select_fields) = node
            .select_stmt()
            .table
            .field_list()
            .map(|fl_vector| {
                fl_vector.iter().filter_map(|fl| match fl {
                    FieldList::Col(col) => Some(col),
                    _ => None,
                })
            })
            .map(|columns| {
                columns.map(|col| {
                    Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id(format!("qbds{}", table_name_first_letter_uppercased)),
                            "addSelectionField".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                table_name_first_letter_uppercased.to_owned(),
                                col.name.to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    }
                    .into()
                })
            })
        {
            statement_list.extend(statement_list_select_fields);
        }
        let local_body = self.local_body_query_ast.get_or_insert(Default::default());
        local_body.stmt_list.extend(statement_list);
        local_body.dcl_list.push(DeclarationStmt {
            decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                declaration: Declaration {
                    decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                    id: format!("qbds{}", uppercase_first_letter(self.table_name_in_select)),
                },
                assignment_clause: None,
            }),
        });

        true
    }

    fn enter_visit_conditional_expr_data(&mut self, node: &'a ConditionalExpressionData) -> bool {
        self.current_cond_expr_data_level += 1;
        match (&*node.cond_expr_left, &node.log_op, &*node.cond_expr_right) {
            (
                ConditionalExpression::Expression(expr_left),
                log_op,
                ConditionalExpression::Expression(expr_right),
            ) => {
                match (expr_left, expr_right) {
                    (
                        Expression::Comparison(ComparisonExpression {
                            simple_expr_left:
                                SimpleExpression::Term(Term::ComplementFactor(
                                    ComplementFactor::SecondFactor(SecondFactor {
                                        sign_op: None,
                                        factor:
                                            Factor::Field(Field::QualifierId(
                                                Qualifier::Id(expr_left_table_name_left),
                                                expr_left_field_name_left,
                                            )),
                                    }),
                                )),
                            comp_op: ComparisonOperator::Equal,
                            simple_expr_right:
                                SimpleExpression::Term(Term::ComplementFactor(
                                    ComplementFactor::SecondFactor(SecondFactor {
                                        sign_op: None,
                                        factor: Factor::Constant(expr_left_const_right),
                                    }),
                                )),
                        }),
                        // right expression
                        Expression::Comparison(ComparisonExpression {
                            simple_expr_left:
                                SimpleExpression::Term(Term::ComplementFactor(
                                    ComplementFactor::SecondFactor(SecondFactor {
                                        sign_op: None,
                                        factor:
                                            Factor::Field(Field::QualifierId(
                                                Qualifier::Id(expr_right_table_name_left),
                                                expr_right_field_name_left,
                                            )),
                                    }),
                                )),
                            comp_op: ComparisonOperator::Equal,
                            simple_expr_right:
                                SimpleExpression::Term(Term::ComplementFactor(
                                    ComplementFactor::SecondFactor(SecondFactor {
                                        sign_op: None,
                                        factor: Factor::Constant(expr_right_const_right),
                                    }),
                                )),
                        }),
                    ) => {
                        /*if expr_left_table_name_left == expr_right_table_name_left
                        && expr_left_field_name_left != expr_right_field_name_left {*/
                        let expr_left_table_name_left_first_letter_uppercased =
                            uppercase_first_letter(expr_left_table_name_left);

                        let expr_right_table_name_left_first_letter_uppercased =
                            uppercase_first_letter(expr_right_table_name_left);

                        self.cond_expr_str_fmt_ctx
                            .push_str_for_str_fmt("(")
                            .push_parm_elem_for_str_fmt(ParmElem::TernaryExpression(Box::new(
                                Factor::Intrinsics(Intrinsics::FieldStr(
                                    expr_left_table_name_left_first_letter_uppercased.clone(),
                                    expr_left_field_name_left.into(),
                                ))
                                .builder()
                                .into(),
                            )))
                            .push_str_for_str_fmt(" == ")
                            .push_parm_elem_for_str_fmt(ParmElem::TernaryExpression(Box::new(
                                Factor::Function(Function::QueryValue(
                                    expr_left_const_right.into_parm_elem_for_query_value(),
                                ))
                                .builder()
                                .into(),
                            )))
                            .push_str_for_str_fmt(")")
                            .push_str_for_str_fmt(format!(" {} ", log_op).as_str())
                            .push_str_for_str_fmt("(")
                            .push_parm_elem_for_str_fmt(ParmElem::TernaryExpression(Box::new(
                                Factor::Intrinsics(Intrinsics::FieldStr(
                                    expr_right_table_name_left_first_letter_uppercased,
                                    expr_right_field_name_left.into(),
                                ))
                                .builder()
                                .into(),
                            )))
                            .push_str_for_str_fmt(" == ")
                            .push_parm_elem_for_str_fmt(ParmElem::TernaryExpression(Box::new(
                                Factor::Function(Function::QueryValue(
                                    expr_right_const_right.into_parm_elem_for_query_value(),
                                ))
                                .builder()
                                .into(),
                            )))
                            .push_str_for_str_fmt(")");

                        /*} else {
                            true
                        }*/
                    }
                    (
                        Expression::Comparison(ComparisonExpression {
                            simple_expr_left:
                                SimpleExpression::Term(Term::ComplementFactor(
                                    ComplementFactor::SecondFactor(SecondFactor {
                                        sign_op: None,
                                        factor:
                                            Factor::Field(Field::QualifierId(
                                                Qualifier::Id(expr_left_table_name_left),
                                                expr_left_field_name_left,
                                            )),
                                    }),
                                )),
                            comp_op: ComparisonOperator::Equal,
                            simple_expr_right:
                                SimpleExpression::Term(Term::ComplementFactor(
                                    ComplementFactor::SecondFactor(SecondFactor {
                                        sign_op: None,
                                        factor: Factor::Constant(expr_left_const_right),
                                    }),
                                )),
                        }),
                        // right expression
                        Expression::Simple(SimpleExpression::Term(Term::ComplementFactor(
                            ComplementFactor::SecondFactor(SecondFactor {
                                sign_op: None,
                                factor: Factor::ParenthesizedTernary(tern_expr_right),
                            }),
                        ))),
                    ) => {
                        let table_name_first_letter_uppercased =
                            uppercase_first_letter(expr_left_table_name_left);
                        self.cond_expr_str_fmt_ctx
                            .push_str_for_str_fmt("(")
                            .push_parm_elem_for_str_fmt(ParmElem::TernaryExpression(Box::new(
                                Factor::Intrinsics(Intrinsics::FieldStr(
                                    table_name_first_letter_uppercased,
                                    expr_left_field_name_left.into(),
                                ))
                                .builder()
                                .into(),
                            )))
                            .push_str_for_str_fmt(" == ")
                            .push_parm_elem_for_str_fmt(ParmElem::TernaryExpression(Box::new(
                                Factor::Function(Function::QueryValue(
                                    expr_left_const_right.into_parm_elem_for_query_value(),
                                ))
                                .builder()
                                .into(),
                            )))
                            .push_str_for_str_fmt(")")
                            .push_str_for_str_fmt(format!(" {} ", log_op).as_str())
                            .push_str_for_str_fmt("(");
                        tern_expr_right.take_visitor_enter_leave(self);
                        self.cond_expr_str_fmt_ctx.push_str_for_str_fmt(")");

                        return false;
                    }
                    _ => {
                        // TODO
                    }
                }
            }
            (
                ConditionalExpression::ConditionalExpression(_cond_expr_left),
                _log_op,
                ConditionalExpression::Expression(_expr_right),
            ) => {
                self.cond_expr_str_fmt_ctx.push_str_for_str_fmt("(");
            }
            _ => {
                // TODO
            }
        }
        match (
            &node.cond_expr_left.parenthesized_inner(),
            &node.log_op,
            &node.cond_expr_right.parenthesized_inner(),
        ) {
            (
                Some(TernaryExpression::ConditionalExpression(cond_expr_left)),
                log_op,
                Some(TernaryExpression::ConditionalExpression(cond_expr_right)),
            ) => {
                self.cond_expr_str_fmt_ctx.push_str_for_str_fmt("(");
                cond_expr_left.take_visitor_enter_leave(self);
                self.cond_expr_str_fmt_ctx
                    .push_str_for_str_fmt(")")
                    .push_str_for_str_fmt(format!(" {log_op} ").as_str());

                self.cond_expr_str_fmt_ctx.push_str_for_str_fmt("(");
                cond_expr_right.take_visitor_enter_leave(self);
                self.cond_expr_str_fmt_ctx.push_str_for_str_fmt(")");
                return false;
            }
            _ => {}
        }
        true
    }

    fn leave_visit_conditional_expr_data(&mut self, node: &'a ConditionalExpressionData) -> bool {
        self.current_cond_expr_data_level -= 1;
        match (&*node.cond_expr_left, &node.log_op, &*node.cond_expr_right) {
            (
                ConditionalExpression::ConditionalExpression(_cond_expr_left),
                log_op,
                ConditionalExpression::Expression(expr_right),
            ) => {
                self.cond_expr_str_fmt_ctx
                    .push_str_for_str_fmt(")")
                    .push_str_for_str_fmt(format!(" {} ", log_op).as_str())
                    .push_str_for_str_fmt("(");

                match expr_right {
                    Expression::Comparison(ComparisonExpression {
                        simple_expr_left:
                            SimpleExpression::Term(Term::ComplementFactor(
                                ComplementFactor::SecondFactor(SecondFactor {
                                    sign_op: None,
                                    factor:
                                        Factor::Field(Field::QualifierId(
                                            Qualifier::Id(expr_right_table_name_left),
                                            expr_right_field_name_left,
                                        )),
                                }),
                            )),
                        comp_op: ComparisonOperator::Equal,
                        simple_expr_right:
                            SimpleExpression::Term(Term::ComplementFactor(
                                ComplementFactor::SecondFactor(SecondFactor {
                                    sign_op: None,
                                    factor: expr_right_factor_right,
                                }),
                            )),
                    }) => {
                        if self.cond_expr_str_fmt_ctx.has_parm_elems_for_str_fmt() {
                            self.cond_expr_str_fmt_ctx
                                .push_parm_elem_for_str_fmt(ParmElem::TernaryExpression(Box::new(
                                    Factor::Intrinsics(Intrinsics::FieldStr(
                                        uppercase_first_letter(expr_right_table_name_left),
                                        expr_right_field_name_left.into(),
                                    ))
                                    .builder()
                                    .into(),
                                )))
                                .push_str_for_str_fmt(" == ")
                                .push_parm_elem_for_str_fmt(ParmElem::TernaryExpression(Box::new(
                                    Factor::Function(Function::QueryValue(
                                        ParmElem::TernaryExpression(Box::new(
                                            expr_right_factor_right.clone().builder().into(),
                                        )),
                                    ))
                                    .builder()
                                    .into(),
                                )));
                        }
                    }
                    _ => {
                        unimplemented!()
                    }
                };

                self.cond_expr_str_fmt_ctx.push_str_for_str_fmt(")");
            }
            _ => {}
        }

        if self.current_cond_expr_data_level == 0 {
            let mut cond_expr_ctx_visitor = ConditionalExpressionContextVisitor::new();
            node.take_visitor_enter_leave(&mut cond_expr_ctx_visitor);
            self.cond_expr_ctx_resolve_strategy =
                cond_expr_ctx_visitor.cond_expr_ctx_resolve_strategy;
            if self.is_in_join_ctx()
                && self.cond_expr_ctx_resolve_strategy
                    == Some(ConditionalExpressionContextResolveStrategy::StrFmt)
            {
                self.join_cond_expr_ctx.insert_cond_expr_strfmt_ctx(
                    self.current_join_table,
                    mem::take(&mut self.cond_expr_str_fmt_ctx),
                );
            }
        }
        true
    }
}

impl<'a> Visitor<'a> for TranspilerVisitor<'a> {
    fn visit_find_join(&mut self, node: &'a FindJoin) {
        self.is_while_select = node.is_while_select();
    }

    fn visit_find_where(&mut self, _node: &'a FindWhere) {}

    fn visit_find_order(&mut self, _node: &'a FindOrder) {}

    fn visit_find_using(&mut self, _node: &'a FindUsing) {}

    fn visit_find_table(&mut self, _node: &'a FindTable) {}

    fn visit_select_table(&mut self, _node: &'a SelectTable) {}

    fn visit_select_stmt(&mut self, node: &'a SelectStmt) {
        self.table_name_in_select = node.table_name();
        self.table_names_from_root_to_children
            .push(self.table_name_in_select);

        if let Some(fo) = node.select_opts.as_ref().and_then(|sel_ops| {
            sel_ops.iter().find_map(|so| match so {
                SelectOpts::FirstOnly(fo) => Some(fo),
                _ => None,
            })
        }) {
            self.select_first_only.insert(node.table_name(), fo);
        }
    }

    fn visit_select_opts(&mut self, _node: &'a SelectOpts) {}

    fn visit_table(&mut self, _node: &'a Table) {}

    fn visit_from(&mut self, _node: &'a From) {}

    fn visit_index_hint(&mut self, _node: &'a IndexHint) {}

    fn visit_table_with_fields(&mut self, _node: &'a TableWithFields) {}

    fn visit_join_spec(&mut self, _node: &'a JoinSpec) {}

    fn visit_join_variant(&mut self, _node: &'a JoinVariant) {}

    fn visit_join_clause(&mut self, node: &'a JoinClause) {
        self.current_join_table = node.table_name();
        self.table_names_from_root_to_children
            .push(self.current_join_table);
        match node.join_variant {
            Some(JoinVariant::Outer) | None => {
                self.table_names_in_joins.push(node.table_name());
            }
            Some(JoinVariant::Exists) | Some(JoinVariant::NotExists) => {} // nothing to do
        }

        if let Some(sel_ops) = node.select_opts.as_ref() {
            let found_first_only = sel_ops
                .iter()
                .filter_map(|so| match so {
                    SelectOpts::FirstOnly(fo) => Some(fo),
                    _ => None,
                })
                .next();
            if let Some(fo) = found_first_only {
                self.select_first_only.insert(node.table_name(), fo);
            }
        }
    }

    fn visit_join_using(&mut self, _node: &'a JoinUsing) {}

    fn visit_join_order(&mut self, _node: &'a JoinOrder) {}

    fn visit_order_group_data(&mut self, _node: &'a OrderGroupData) {}

    fn visit_order_group(&mut self, _node: &'a OrderGroup) {}

    fn visit_group_order_by(&mut self, _node: &'a GroupOrderBy) {}

    fn visit_order_elem(&mut self, _node: &'a OrderElem) {}

    fn visit_direction(&mut self, _node: &'a Direction) {}

    fn visit_first_only(&mut self, _node: &'a FirstOnly) {}

    fn visit_force_placeholder_literal(&mut self, _node: &'a ForcePlaceholderLiteral) {}

    fn visit_function_expr(&mut self, _node: &'a FunctionExpression) {}

    fn visit_column(&mut self, _node: &'a Column) {}

    fn visit_conditional_expr(&mut self, _node: &'a ConditionalExpression) {}

    fn visit_conditional_expr_data(&mut self, _node: &'a ConditionalExpressionData) {}

    fn visit_expr(&mut self, _node: &'a Expression) {
        if self.is_in_join_ctx() {
            self.num_of_expressions_in_where_in_current_join_ctx += 1;
        }
    }

    fn visit_comparison_expr(&mut self, node: &'a ComparisonExpression) {
        let mut eval_name: Option<EvalName> = None;
        let mut range_value: Option<ParmElem> = None;
        let is_in_join_ctx = self.is_in_join_ctx();

        match (
            &node.simple_expr_left,
            &node.comp_op,
            &node.simple_expr_right,
        ) {
            (
                SimpleExpression::SimpleExpression(_simple_expr_data_left),
                _,
                SimpleExpression::SimpleExpression(_simple_expr_data_right),
            ) => {
                // TODO: `terms` are not implemented yet
            }
            (
                SimpleExpression::Term(Term::ComplementFactor(ComplementFactor::NotSecondFactor(
                    _not_scnd_fact_left,
                ))),
                _,
                SimpleExpression::Term(Term::ComplementFactor(ComplementFactor::NotSecondFactor(
                    _not_scnd_fact_right,
                ))),
            ) => {
                // TODO: implement this
            }
            (
                SimpleExpression::Term(Term::ComplementFactor(ComplementFactor::SecondFactor(
                    scnd_fact_left,
                ))),
                ComparisonOperator::Equal,
                SimpleExpression::Term(Term::ComplementFactor(ComplementFactor::SecondFactor(
                    scnd_fact_right,
                ))),
            ) => {
                match (&scnd_fact_left.factor, &scnd_fact_right.factor) {
                    (Factor::Field(
                        field
                    ), Factor::Constant(
                        constant
                    )) // we have a reflexive relationship, due to `==` operator
                    | (Factor::Constant(
                        constant
                    ), Factor::Field(
                        field
                    )) => {
                        if let Ok(intrinsics_field_num) = field.try_mk_intrinsics_field_num() {
                            eval_name = Some(EvalName::Qualifier(
                                Qualifier::Eval(Box::new(Eval {
                                    eval_name: EvalName::Qualifier(
                                        Qualifier::Id(
                                            format!("qbds{}", 
                                                intrinsics_field_num.table_name_and_field_name().0)),
                                        "addRange".to_owned()
                                    ),
                                    parm_list: Some(
                                        vec![
                                            ParmElem::TernaryExpression(
                                                Box::new(Factor::Intrinsics(
                                                    intrinsics_field_num
                                                ).builder().into())
                                            )
                                        ]
                                    )
                                })),
                                "value".to_owned()));
                        }
                        range_value = Some(constant.into_parm_elem_for_query_value());
                    },
                    (Factor::Field(
                        field_left
                    ),
                    Factor::Field(
                        field_right
                    )) => {
                        match (field_left, field_right) {
                            (
                                Field::QualifierId(
                                    Qualifier::Id(table_name_left),
                                    _field_name_left
                                ),
                                Field::QualifierId(
                                    Qualifier::Id(table_name_right),
                                    _field_name_right
                                )
                            ) => {
                                if table_name_left == self.current_join_table || table_name_right == self.current_join_table {
                                    self.join_link_fields_ctx.insert(self.current_join_table, (field_left, field_right));
                                }
                            },
                            _ => {
                                todo!();
                            }
                        }
                    }
                    _ => {
                        // TODO: implement the other cases!
                        unimplemented!();
                    }
                }
            }
            (
                SimpleExpression::Term(Term::Term(_term_data_left)),
                _,
                SimpleExpression::Term(Term::Term(_term_data_right)),
            ) => {
                // TODO: `terms` are not implemented yet
                unimplemented!();
            }
            _ => {
                // TODO: implement other cases
                unimplemented!();
            }
        }

        if let (Some(eval_name), Some(range_value)) = (eval_name, range_value) {
            let eval = Eval {
                eval_name,
                parm_list: Some(vec![range_value]),
            };
            if is_in_join_ctx {
                self.join_cond_expr_ctx
                    .insert_eval_add_range_value(self.current_join_table, eval);
            } else {
                self.cond_expr_add_range_stmts.push(eval.into());
            }
        }
    }

    fn visit_as_expr(&mut self, _node: &'a AsExpression) {}

    fn visit_is_expr(&mut self, _node: &'a IsExpression) {}

    fn visit_simple_expr(&mut self, _node: &'a SimpleExpression) {}

    fn visit_simple_expr_data(&mut self, _node: &'a SimpleExpressionData) {}

    fn visit_term(&mut self, _node: &'a Term) {}

    fn visit_term_data(&mut self, _node: &'a TermData) {}

    fn visit_complement_factor(&mut self, _node: &'a ComplementFactor) {}

    fn visit_second_factor(&mut self, _node: &'a SecondFactor) {}

    fn visit_factor(&mut self, _node: &'a Factor) {}

    fn visit_constant(&mut self, _node: &'a Constant) {}

    fn visit_field(&mut self, _node: &'a Field) {}

    fn visit_function(&mut self, _node: &'a Function) {}

    fn visit_parm_elem(&mut self, _node: &'a ParmElem) {}

    fn visit_intrinsics(&mut self, _node: &'a Intrinsics) {}

    fn visit_eval(&mut self, _node: &'a Eval) {}

    fn visit_eval_name(&mut self, _node: &'a EvalName) {}

    fn visit_if_expr(&mut self, _node: &'a IfExpression) {}

    fn visit_qualifier(&mut self, _node: &'a Qualifier) {}

    fn visit_ternary_expr(&mut self, _node: &'a TernaryExpression) {}

    fn visit_logical_op(&mut self, _node: &'a LogicalOperator) {}

    fn visit_comparison_op(&mut self, _node: &'a ComparisonOperator) {}

    fn visit_field_list(&mut self, _node: &'a FieldList) {}

    fn visit_dcl_stmt(&mut self, _: &'a dracaena::ast::declaration::DeclarationStmt) {}
    fn visit_dcl_init_list(&mut self, _: &'a dracaena::ast::declaration::DeclarationInitList) {}
    fn visit_dcl_comma_list(&mut self, _: &'a dracaena::ast::declaration::DeclarationCommaList) {}
    fn visit_dcl_init(&mut self, _: &'a dracaena::ast::declaration::DeclarationInit) {}
    fn visit_dcl(&mut self, _: &'a dracaena::ast::declaration::Declaration) {}
    fn visit_assign_clause(&mut self, _: &'a dracaena::ast::assignment::AssignmentClause) {}
    fn visit_assign_stmt(&mut self, _: &'a dracaena::ast::assignment::AssignmentStmt) {}
    fn visit_id_with_assign_clause(
        &mut self,
        _: &'a dracaena::ast::declaration::IdWithAssigmentClause,
    ) {
    }
    fn visit_assign(&mut self, _: &'a dracaena::ast::assignment::Assign) {}
    fn visit_pre_post_inc_dec_assign(
        &mut self,
        _: &'a dracaena::ast::assignment::PrePostIncDecAssign,
    ) {
    }
    fn visit_assign_inc_dec(&mut self, _: &'a dracaena::ast::assignment::AssignIncDec) {}
    fn visit_stmt(&mut self, _: &'a dracaena::ast::statement::Statement) {}
    fn visit_expr_stmt(&mut self, _: &'a dracaena::ast::statement::ExpressionStatement) {}
    fn visit_local_body(&mut self, _: &'a dracaena::ast::local_body::LocalBody) {}
}

trait IntoParmElemQueryValue {
    fn into_parm_elem_for_query_value(self) -> ParmElem;
}

impl IntoParmElemQueryValue for Constant {
    fn into_parm_elem_for_query_value(self) -> ParmElem {
        match &self {
            Constant::String(_) => {
                ParmElem::TernaryExpression(Box::new(Factor::Constant(self).builder().into()))
            }
            Constant::Double(_)
            | Constant::Integer(_)
            | Constant::Integer64(_)
            | Constant::False
            | Constant::True
            | Constant::Enum(_, _) => {
                ParmElem::TernaryExpression(Box::new(Factor::Constant(self).builder().into()))
            }
            Constant::Null | Constant::TableMap(_, _, _) => {
                // TODO: we don't know how to handle these yet
                unimplemented!()
            }
        }
    }
}

impl IntoParmElemQueryValue for &Constant {
    fn into_parm_elem_for_query_value(self) -> ParmElem {
        self.clone().into_parm_elem_for_query_value()
    }
}

trait JoinableExpression {
    fn is_joinable_expression(&self) -> bool;
}

impl JoinableExpression for ComparisonExpression {
    fn is_joinable_expression(&self) -> bool {
        match self {
            ComparisonExpression {
                simple_expr_left:
                    SimpleExpression::Term(Term::ComplementFactor(ComplementFactor::SecondFactor(
                        SecondFactor {
                            sign_op: _sign_op_left,
                            factor:
                                Factor::Field(Field::QualifierId(
                                    Qualifier::Id(_table_name_left),
                                    _field_name_left,
                                )),
                        },
                    ))),
                comp_op: ComparisonOperator::Equal,
                simple_expr_right:
                    SimpleExpression::Term(Term::ComplementFactor(ComplementFactor::SecondFactor(
                        SecondFactor {
                            sign_op: _sign_op_right,
                            factor:
                                Factor::Field(Field::QualifierId(
                                    Qualifier::Id(_table_name_right),
                                    _field_name_right,
                                )),
                        },
                    ))),
            } => true,
            _ => false,
        }
    }
}

#[derive(Debug, Default)]
struct JoinStatements<'a> {
    join_table_statements: HashMap<&'a str, Vec<Statement>>,
}

impl<'a> JoinStatements<'a> {
    pub fn insert_many(
        &mut self,
        join_table: &'a str,
        stmnts: impl IntoIterator<Item = Statement>,
    ) {
        let statements = self
            .join_table_statements
            .entry(join_table)
            .or_insert(vec![]);
        statements.extend(stmnts);
    }

    pub fn inner_into_statements(&mut self, join_table: &'a str) -> Vec<Statement> {
        self.join_table_statements
            .remove(join_table)
            .unwrap_or_default()
    }
}

#[derive(Debug, Default)]
struct JoinLinkFieldsContext<'a> {
    join_table_name_with_linked_fields: HashMap<&'a str, Vec<(&'a Field, &'a Field)>>,
    tables_that_added_data_sources: HashMap<String, Vec<String>>, // TODO: use references here
    added_join_tables_as_data_source: HashSet<&'a str>,
    siblings_of_join_table: HashMap<String, HashSet<String>>, // TODO: use references here
}

impl<'a> JoinLinkFieldsContext<'a> {
    pub fn insert(&mut self, join_table: &'a str, field_link: (&'a Field, &'a Field)) {
        if let Some(vec_fields) = self.join_table_name_with_linked_fields.get_mut(join_table) {
            vec_fields.push(field_link);
        } else {
            self.join_table_name_with_linked_fields
                .insert(join_table, vec![field_link]);
        }
    }

    pub fn num_of_join_links(&self, join_table: &'a str) -> usize {
        self.join_table_name_with_linked_fields
            .get(join_table)
            .map_or(0, |links| links.len())
    }

    /// This must only be used after `inner_into_statements` has been called
    /// FIXME: this is really bad design, because one has to remember the ordering!
    pub fn table_is_joined_sibling(&self, join_table: &'a str) -> bool {
        self.siblings_of_join_table
            .get(join_table)
            .map_or(false, |siblings| !siblings.is_empty())
    }

    fn is_child_only_linked_with_parent(&self, child: &'a str, parent: &'a str) -> bool {
        self.join_table_name_with_linked_fields
            .get(child)
            .map(|vec_fields| {
                vec_fields
                    .iter()
                    .map(|(field_left, field_right)| {
                        (
                            field_left.try_mk_intrinsics_field_num(),
                            field_right.try_mk_intrinsics_field_num(),
                        )
                    })
                    .map(|(field_left_res, field_right_res)| {
                        match (field_left_res, field_right_res) {
                            (Ok(field_left), Ok(field_right)) => (
                                field_left.into_table_name_and_field_name().0,
                                field_right.into_table_name_and_field_name().0,
                            ),
                            _ => todo!(),
                        }
                    })
                    .all(|(table_name_left, table_name_right)| {
                        if table_name_left.eq_ignore_ascii_case(child) {
                            table_name_right.eq_ignore_ascii_case(parent)
                        } else {
                            table_name_left.eq_ignore_ascii_case(parent)
                        }
                    })
            })
            .unwrap_or(false)
    }

    pub fn inner_into_statements(
        &mut self,
        join_table: &'a str,
        table_names_from_root_to_children: &[&'a str],
    ) -> Vec<Statement> {
        use indexmap::IndexSet;

        if let Some(linked_fields_list) = self.join_table_name_with_linked_fields.remove(join_table)
        {
            let intrinsic_field_nums =
                linked_fields_list.iter().map(|(field_left, field_right)| {
                    (
                        (*field_left).try_mk_intrinsics_field_num(),
                        (*field_right).try_mk_intrinsics_field_num(),
                    )
                });

            let tables_linked_to_join_table: IndexSet<String> = intrinsic_field_nums
                .map(
                    |(intr_left_res, intr_right_res)| match (intr_left_res, intr_right_res) {
                        (Ok(intr_left), Ok(intr_right)) => {
                            if intr_left.table_name_and_field_name().0
                                == uppercase_first_letter(join_table)
                            {
                                intr_right.into_table_name_and_field_name().0
                            } else {
                                intr_left.into_table_name_and_field_name().0
                            }
                        }
                        _ => {
                            todo!();
                        }
                    },
                )
                .collect();

            let parent_tables_slice = table_names_from_root_to_children
                .iter()
                .enumerate()
                .find_map(|(idx, tn)| if *tn == join_table { Some(idx) } else { None })
                .and_then(|idx| table_names_from_root_to_children.get(..idx - 1_usize));

            let (mut tables_needing_add_link, mut tables_needing_add_data_source): (
                Vec<String>,
                Vec<String>,
            ) = tables_linked_to_join_table
                .into_iter()
                .partition(|table_linked_to_join_table| {
                    parent_tables_slice
                        .map(|slice| {
                            slice
                                .iter()
                                .map(|table_name| uppercase_first_letter(table_name))
                                .filter(|table_name| {
                                    self.is_child_only_linked_with_parent(join_table, table_name)
                                })
                                .any(|tn| &tn == &table_linked_to_join_table.as_str())
                        })
                        .unwrap_or(false)
                });

            if let Some((last, rest)) = tables_needing_add_data_source.split_last() {
                tables_needing_add_link.extend(rest.to_vec());
                tables_needing_add_data_source = vec![last.clone()];
            }

            let add_data_source_stmts: Vec<Statement> = if !self
                .added_join_tables_as_data_source
                .contains(join_table)
            {
                self.added_join_tables_as_data_source.insert(join_table);
                tables_needing_add_data_source
                    .into_iter()
                    .map(|parent| {
                        if let Some(children) =
                            self.tables_that_added_data_sources.get_mut(parent.as_str())
                        {
                            self.siblings_of_join_table
                                .entry(join_table.into())
                                .and_modify(|siblings| {
                                    // FIXME: ouch, that hurts!
                                    *siblings = children.clone().into_iter().collect();
                                })
                                .or_default();

                            children.push(join_table.into());

                            for child in children.iter() {
                                // FIXME: ouch, that hurts!
                                self.siblings_of_join_table.insert(
                                    child.to_string(),
                                    children.clone().into_iter().collect(),
                                );
                            }
                        } else {
                            self.tables_that_added_data_sources
                                .insert(parent.clone(), vec![join_table.into()]);
                        }
                        AssignmentStmt::FieldAssign {
                            field: Field::Id(format!("qbds{}", uppercase_first_letter(join_table))),
                            assign: Assign::AssignEqual,
                            tern_expr: Factor::Eval(Eval {
                                eval_name: EvalName::Qualifier(
                                    Qualifier::Id(format!(
                                        "qbds{}",
                                        uppercase_first_letter(parent.as_ref())
                                    )),
                                    "addDataSource".to_owned(),
                                ),
                                parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                                    Factor::Field(Field::QualifierId(
                                        Qualifier::Id(join_table.into()),
                                        "TableId".to_owned(),
                                    ))
                                    .builder()
                                    .into(),
                                ))]),
                            })
                            .builder()
                            .into(),
                        }
                        .into()
                    })
                    .collect()
            } else {
                Vec::new()
            };

            let add_link_stmts =
                linked_fields_list.iter().map(|(field_left, field_right)| {
                    match (field_left, field_right) {
                        (
                            Field::QualifierId(Qualifier::Id(table_name_left), field_name_left),
                            Field::QualifierId(Qualifier::Id(table_name_right), field_name_right),
                        ) => {
                            let (parent_table, parent_field, child_table, child_field) =
                                if table_name_left == join_table {
                                    (
                                        uppercase_first_letter(table_name_right),
                                        field_name_right,
                                        uppercase_first_letter(table_name_left),
                                        field_name_left,
                                    )
                                } else {
                                    (
                                        uppercase_first_letter(table_name_left),
                                        field_name_left,
                                        uppercase_first_letter(table_name_right),
                                        field_name_right,
                                    )
                                };
                            let mut eval = Eval {
                                eval_name: EvalName::Qualifier(
                                    Qualifier::Id(format!("qbds{}", child_table.as_str())),
                                    "addLink".into(),
                                ),
                                parm_list: Some(vec![
                                    ParmElem::TernaryExpression(Box::new(
                                        Factor::Intrinsics(Intrinsics::FieldNum(
                                            parent_table.clone(),
                                            parent_field.into(),
                                        ))
                                        .builder()
                                        .into(),
                                    )),
                                    ParmElem::TernaryExpression(Box::new(
                                        Factor::Intrinsics(Intrinsics::FieldNum(
                                            child_table,
                                            child_field.into(),
                                        ))
                                        .builder()
                                        .into(),
                                    )),
                                ]),
                            };

                            if tables_needing_add_link.contains(&parent_table) {
                                if let Some(prm_list) = eval.parm_list.as_mut() {
                                    prm_list.push(ParmElem::TernaryExpression(Box::new(
                                        Factor::Eval(Eval {
                                            eval_name: EvalName::Qualifier(
                                                Qualifier::Id(format!("qbds{}", parent_table)),
                                                "name".into(),
                                            ),
                                            parm_list: None,
                                        })
                                        .builder()
                                        .into(),
                                    )));
                                }
                            }
                            eval.into()
                        }
                        _ => {
                            todo!()
                        }
                    }
                });

            add_data_source_stmts
                .into_iter()
                .chain(add_link_stmts)
                .collect()
        } else {
            Vec::new()
        }
    }
}

type AddRangeStatements = Vec<Eval>;

#[derive(Debug, PartialEq, Default)]
struct JoinConditionalExpressionContext<'a> {
    join_table_to_add_range_statements: HashMap<&'a str, AddRangeStatements>,
    join_table_to_cond_expr_str_fmt_ctx: HashMap<&'a str, ConditionalExpressionStrFmtContext>,
}

impl<'a> JoinConditionalExpressionContext<'a> {
    pub fn insert_eval_add_range_value(&mut self, join_table: &'a str, eval_add_range_value: Eval) {
        self.join_table_to_add_range_statements
            .entry(join_table)
            .or_insert(Vec::new())
            .push(eval_add_range_value);
    }

    pub fn insert_cond_expr_strfmt_ctx(
        &mut self,
        join_table: &'a str,
        cond_expr_as_str_fmt: ConditionalExpressionStrFmtContext,
    ) -> Option<ConditionalExpressionStrFmtContext> {
        self.join_table_to_cond_expr_str_fmt_ctx
            .insert(join_table, cond_expr_as_str_fmt)
    }

    pub fn inner_into_statements(
        &mut self,
        join_table: &'a str,
        strategy: ConditionalExpressionContextResolveStrategy,
    ) -> Vec<Statement> {
        match strategy {
            ConditionalExpressionContextResolveStrategy::AddRange => {
                if let Some(eval_add_range_list) =
                    self.join_table_to_add_range_statements.remove(join_table)
                {
                    eval_add_range_list
                        .into_iter()
                        .map(|eval| eval.into())
                        .collect()
                } else {
                    Vec::new()
                }
            }
            ConditionalExpressionContextResolveStrategy::StrFmt => {
                if let Some(cond_expr_str_fmt_ctx) =
                    self.join_table_to_cond_expr_str_fmt_ctx.remove(join_table)
                {
                    vec![Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Eval(Box::new(Eval {
                                eval_name: EvalName::Qualifier(
                                    Qualifier::Id(format!(
                                        "qbds{}",
                                        uppercase_first_letter(join_table)
                                    )),
                                    "addRange".to_owned(),
                                ),
                                parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                                    Factor::Intrinsics(
                                        // this range will have no effect in the end;
                                        // we use DataAreaId here as a convention indicating to the user
                                        // that the range will actually be useles, because it will be
                                        // solely determined by the query range **value**
                                        // see also: https://web.archive.org/web/20190305223625/http://www.axaptapedia.com/Expressions_in_query_ranges
                                        Intrinsics::FieldNum(
                                            uppercase_first_letter(join_table),
                                            "DataAreaId".to_owned(),
                                        ),
                                    )
                                    .builder()
                                    .into(),
                                ))]),
                            })),
                            "value".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Function(cond_expr_str_fmt_ctx.into_function_str_fmt())
                                .builder()
                                .into(),
                        ))]),
                    }
                    .into()]
                } else {
                    Vec::new()
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {

    use super::*;
    use pretty_assertions::assert_eq;
    use selector::parsers::parser::*;

    #[test]
    fn single_select_with_field_and_table() {
        let mut visitor = TranspilerVisitor::new();
        let parse_result = "select RecId from custTable;".parse::<SelectStmtParsed>();

        assert!(
            parse_result.is_ok(),
            "Parsing failed: {}",
            parse_result.unwrap_err()
        );
        let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

        assert_eq!(left_over, "");

        ast.take_visitor_enter_leave(&mut visitor);
        let actual_ast = visitor.into_transpiled_ast();

        let expected_ast = LocalBody {
            dcl_list: vec![
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsCustTable".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("Query".to_owned()),
                            id: "query".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryRun".to_owned()),
                            id: "queryRun".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("CustTable".to_owned()),
                            id: "custTable".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
            ],
            stmt_list: vec![
                AssignmentStmt::FieldAssign {
                    field: Field::Id("query".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::New("Query".to_owned()),
                        parm_list: None,
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsCustTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("query".to_owned()),
                            "addDataSource".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("custTable".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsCustTable".to_owned()),
                        "addSelectionField".to_owned(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Intrinsics(Intrinsics::FieldNum(
                            "CustTable".to_owned(),
                            "RecId".to_owned(),
                        ))
                        .builder()
                        .into(),
                    ))]),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("queryRun".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::New("QueryRun".to_owned()),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::Id("query".to_owned()))
                                .builder()
                                .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
            ],
        };

        assert_eq!(Some(expected_ast), actual_ast);
    }

    #[test]
    fn select_firstonly_table_where_simple_field_comparison() {
        let mut visitor = TranspilerVisitor::new();
        let parse_result = r#"select firstonly custTable
                                where custTable.AccountNum == '4000';"#
            .parse::<SelectStmtParsed>();

        assert!(
            parse_result.is_ok(),
            "Parsing failed: {}",
            parse_result.unwrap_err()
        );
        let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

        assert_eq!(left_over, "");

        ast.take_visitor_enter_leave(&mut visitor);
        let actual_ast = visitor.into_transpiled_ast();

        let expected_ast = LocalBody {
            dcl_list: vec![
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsCustTable".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("Query".to_owned()),
                            id: "query".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryRun".to_owned()),
                            id: "queryRun".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("CustTable".to_owned()),
                            id: "custTable".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
            ],
            stmt_list: vec![
                AssignmentStmt::FieldAssign {
                    field: Field::Id("query".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::New("Query".to_owned()),
                        parm_list: None,
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsCustTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("query".to_owned()),
                            "addDataSource".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("custTable".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsCustTable".to_owned()),
                        "firstOnly".to_owned(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::True).builder().into(),
                    ))]),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Eval(Box::new(Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("qbdsCustTable".to_owned()),
                                "addRange".to_owned(),
                            ),
                            parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                                Factor::Intrinsics(Intrinsics::FieldNum(
                                    "CustTable".to_owned(),
                                    "AccountNum".to_owned(),
                                ))
                                .builder()
                                .into(),
                            ))]),
                        })),
                        "value".to_owned(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::String("4000".to_owned()))
                            .builder()
                            .into(),
                    ))]),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("queryRun".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::New("QueryRun".to_owned()),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::Id("query".to_owned()))
                                .builder()
                                .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("queryRun".to_owned()),
                        "next".to_owned(),
                    ),
                    parm_list: None,
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("custTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "get".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("custTable".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
            ],
        };

        assert_eq!(Some(expected_ast), actual_ast)
    }

    #[test]
    fn select_firstonly_table_where_conditional_and_expression_on_different_fields() {
        let mut visitor = TranspilerVisitor::new();
        let parse_result = r#"select firstonly custTable
                                where custTable.AccountNum == '4000'
                                    && custTable.Party == '12345';"#
            .parse::<SelectStmtParsed>();

        assert!(
            parse_result.is_ok(),
            "Parsing failed: {}",
            parse_result.unwrap_err()
        );
        let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

        assert_eq!(left_over, "");

        ast.take_visitor_enter_leave(&mut visitor);
        let actual_ast = visitor.into_transpiled_ast();

        let expected_ast = LocalBody {
            dcl_list: vec![
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsCustTable".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("Query".to_owned()),
                            id: "query".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryRun".to_owned()),
                            id: "queryRun".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("CustTable".to_owned()),
                            id: "custTable".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
            ],
            stmt_list: vec![
                AssignmentStmt::FieldAssign {
                    field: Field::Id("query".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::New("Query".to_owned()),
                        parm_list: None,
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsCustTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("query".to_owned()),
                            "addDataSource".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("custTable".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsCustTable".to_owned()),
                        "firstOnly".to_owned(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::True).builder().into(),
                    ))]),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Eval(Box::new(Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("qbdsCustTable".to_owned()),
                                "addRange".to_owned(),
                            ),
                            parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                                Factor::Intrinsics(Intrinsics::FieldNum(
                                    "CustTable".to_owned(),
                                    "AccountNum".to_owned(),
                                ))
                                .builder()
                                .into(),
                            ))]),
                        })),
                        "value".to_owned(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::String("4000".to_owned()))
                            .builder()
                            .into(),
                    ))]),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Eval(Box::new(Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("qbdsCustTable".to_owned()),
                                "addRange".to_owned(),
                            ),
                            parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                                Factor::Intrinsics(Intrinsics::FieldNum(
                                    "CustTable".to_owned(),
                                    "Party".to_owned(),
                                ))
                                .builder()
                                .into(),
                            ))]),
                        })),
                        "value".to_owned(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::String("12345".to_owned()))
                            .builder()
                            .into(),
                    ))]),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("queryRun".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::New("QueryRun".to_owned()),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::Id("query".to_owned()))
                                .builder()
                                .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("queryRun".to_owned()),
                        "next".to_owned(),
                    ),
                    parm_list: None,
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("custTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "get".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("custTable".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
            ],
        };

        assert_eq!(Some(expected_ast), actual_ast)
    }

    #[test]
    fn select_firstonly_table_where_conditional_or_expression_on_same_fields() {
        let mut visitor = TranspilerVisitor::new();
        let parse_result = r#"select firstonly custTable
                                where custTable.AccountNum == '4000'
                                    || custTable.AccountNum == '4001';"#
            .parse::<SelectStmtParsed>();

        assert!(
            parse_result.is_ok(),
            "Parsing failed: {}",
            parse_result.unwrap_err()
        );
        let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

        assert_eq!(left_over, "");

        ast.take_visitor_enter_leave(&mut visitor);
        let actual_ast = visitor.into_transpiled_ast();

        let expected_ast = LocalBody {
            dcl_list: vec![
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsCustTable".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("Query".to_owned()),
                            id: "query".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryRun".to_owned()),
                            id: "queryRun".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("CustTable".to_owned()),
                            id: "custTable".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
            ],
            stmt_list: vec![
                AssignmentStmt::FieldAssign {
                    field: Field::Id("query".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::New("Query".to_owned()),
                        parm_list: None,
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsCustTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("query".to_owned()),
                            "addDataSource".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("custTable".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsCustTable".to_owned()),
                        "firstOnly".to_owned(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::True).builder().into(),
                    ))]),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Eval(Box::new(Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("qbdsCustTable".to_owned()),
                                "addRange".to_owned(),
                            ),
                            parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                                Factor::Intrinsics(Intrinsics::FieldNum(
                                    "CustTable".to_owned(),
                                    "AccountNum".to_owned(),
                                ))
                                .builder()
                                .into(),
                            ))]),
                        })),
                        "value".to_owned(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::String("4000".to_owned()))
                            .builder()
                            .into(),
                    ))]),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Eval(Box::new(Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("qbdsCustTable".to_owned()),
                                "addRange".to_owned(),
                            ),
                            parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                                Factor::Intrinsics(Intrinsics::FieldNum(
                                    "CustTable".to_owned(),
                                    "AccountNum".to_owned(),
                                ))
                                .builder()
                                .into(),
                            ))]),
                        })),
                        "value".to_owned(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::String("4001".to_owned()))
                            .builder()
                            .into(),
                    ))]),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("queryRun".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::New("QueryRun".to_owned()),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::Id("query".to_owned()))
                                .builder()
                                .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("queryRun".to_owned()),
                        "next".to_owned(),
                    ),
                    parm_list: None,
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("custTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "get".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("custTable".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
            ],
        };

        assert_eq!(Some(expected_ast), actual_ast)
    }

    #[test]
    fn select_firstonly_table_where_conditional_or_expression_on_different_fields() {
        let mut visitor = TranspilerVisitor::new();
        let parse_result = r#"select firstonly custTable
                                where custTable.AccountNum == '4000'
                                    || custTable.Name == 'Foo';"#
            .parse::<SelectStmtParsed>();

        assert!(
            parse_result.is_ok(),
            "Parsing failed: {}",
            parse_result.unwrap_err()
        );
        let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

        assert_eq!(left_over, "");

        ast.take_visitor_enter_leave(&mut visitor);
        let actual_ast = visitor.into_transpiled_ast();

        let expected_ast = LocalBody {
            dcl_list: vec![
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsCustTable".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("Query".to_owned()),
                            id: "query".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryRun".to_owned()),
                            id: "queryRun".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("CustTable".to_owned()),
                            id: "custTable".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
            ],
            stmt_list: vec![
                AssignmentStmt::FieldAssign {
                    field: Field::Id("query".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::New("Query".to_owned()),
                        parm_list: None,
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsCustTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("query".to_owned()),
                            "addDataSource".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("custTable".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsCustTable".to_owned()),
                        "firstOnly".to_owned(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::True).builder().into(),
                    ))]),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Eval(Box::new(Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("qbdsCustTable".to_owned()),
                                "addRange".to_owned(),
                            ),
                            parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                                Factor::Intrinsics(
                                    // this range will have no effect in the end;
                                    // we use DataAreaId here as a convention indicating to the user
                                    // that the range will actually be useles, because it will be
                                    // solely determined by the query range **value**
                                    // see also: https://web.archive.org/web/20190305223625/http://www.axaptapedia.com/Expressions_in_query_ranges
                                    Intrinsics::FieldNum(
                                        "CustTable".to_owned(),
                                        "DataAreaId".to_owned(),
                                    ),
                                )
                                .builder()
                                .into(),
                            ))]),
                        })),
                        "value".to_owned(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Function(Function::StrFmt(vec![
                            ParmElem::TernaryExpression(Box::new(
                                Factor::Constant(Constant::String(
                                    r#"((%1 == "%2") || (%3 == "%4"))"#.into(),
                                ))
                                .builder()
                                .into(),
                            )),
                            ParmElem::TernaryExpression(Box::new(
                                Factor::Intrinsics(Intrinsics::FieldStr(
                                    "CustTable".into(),
                                    "AccountNum".into(),
                                ))
                                .builder()
                                .into(),
                            )),
                            ParmElem::TernaryExpression(Box::new(
                                Factor::Function(Function::QueryValue(
                                    ParmElem::TernaryExpression(Box::new(
                                        Factor::Constant(Constant::String("4000".into()))
                                            .builder()
                                            .into(),
                                    )),
                                ))
                                .builder()
                                .into(),
                            )),
                            ParmElem::TernaryExpression(Box::new(
                                Factor::Intrinsics(Intrinsics::FieldStr(
                                    "CustTable".into(),
                                    "Name".into(),
                                ))
                                .builder()
                                .into(),
                            )),
                            ParmElem::TernaryExpression(Box::new(
                                Factor::Function(Function::QueryValue(
                                    ParmElem::TernaryExpression(Box::new(
                                        Factor::Constant(Constant::String("Foo".into()))
                                            .builder()
                                            .into(),
                                    )),
                                ))
                                .builder()
                                .into(),
                            )),
                        ]))
                        .builder()
                        .into(),
                    ))]),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("queryRun".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::New("QueryRun".to_owned()),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::Id("query".to_owned()))
                                .builder()
                                .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("queryRun".to_owned()),
                        "next".to_owned(),
                    ),
                    parm_list: None,
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("custTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "get".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("custTable".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
            ],
        };

        assert_eq!(Some(expected_ast), actual_ast)
    }

    #[test]
    fn select_firstonly_table_where_conditional_or_and_expression() {
        let mut visitor = TranspilerVisitor::new();
        let parse_result = r#"select firstonly custTable
                                where custTable.AccountNum == '4000'
                                    || custTable.Name == 'Foo'
                                    && custTable.DlvMode == 'Baz';"#
            .parse::<SelectStmtParsed>();

        assert!(
            parse_result.is_ok(),
            "Parsing failed: {}",
            parse_result.unwrap_err()
        );
        let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

        assert_eq!(left_over, "");

        ast.take_visitor_enter_leave(&mut visitor);
        let actual_ast = visitor.into_transpiled_ast();

        let expected_ast = LocalBody {
            dcl_list: vec![
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsCustTable".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("Query".to_owned()),
                            id: "query".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryRun".to_owned()),
                            id: "queryRun".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("CustTable".to_owned()),
                            id: "custTable".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
            ],
            stmt_list: vec![
                AssignmentStmt::FieldAssign {
                    field: Field::Id("query".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::New("Query".to_owned()),
                        parm_list: None,
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsCustTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("query".to_owned()),
                            "addDataSource".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("custTable".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsCustTable".to_owned()),
                        "firstOnly".to_owned(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::True).builder().into(),
                    ))]),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Eval(Box::new(Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("qbdsCustTable".to_owned()),
                                "addRange".to_owned(),
                            ),
                            parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                                Factor::Intrinsics(
                                    // this range will have no effect in the end;
                                    // we use DataAreaId here as a convention indicating to the user
                                    // that the range will actually be useles, because it will be
                                    // solely determined by the query range **value**
                                    // see also: https://web.archive.org/web/20190305223625/http://www.axaptapedia.com/Expressions_in_query_ranges
                                    Intrinsics::FieldNum(
                                        "CustTable".to_owned(),
                                        "DataAreaId".to_owned(),
                                    ),
                                )
                                .builder()
                                .into(),
                            ))]),
                        })),
                        "value".to_owned(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Function(Function::StrFmt(vec![
                            ParmElem::TernaryExpression(Box::new(
                                Factor::Constant(Constant::String(
                                    r#"(((%1 == "%2") || (%3 == "%4")) && (%5 == "%6"))"#.into(),
                                ))
                                .builder()
                                .into(),
                            )),
                            ParmElem::TernaryExpression(Box::new(
                                Factor::Intrinsics(Intrinsics::FieldStr(
                                    "CustTable".into(),
                                    "AccountNum".into(),
                                ))
                                .builder()
                                .into(),
                            )),
                            ParmElem::TernaryExpression(Box::new(
                                Factor::Function(Function::QueryValue(
                                    ParmElem::TernaryExpression(Box::new(
                                        Factor::Constant(Constant::String("4000".into()))
                                            .builder()
                                            .into(),
                                    )),
                                ))
                                .builder()
                                .into(),
                            )),
                            ParmElem::TernaryExpression(Box::new(
                                Factor::Intrinsics(Intrinsics::FieldStr(
                                    "CustTable".into(),
                                    "Name".into(),
                                ))
                                .builder()
                                .into(),
                            )),
                            ParmElem::TernaryExpression(Box::new(
                                Factor::Function(Function::QueryValue(
                                    ParmElem::TernaryExpression(Box::new(
                                        Factor::Constant(Constant::String("Foo".into()))
                                            .builder()
                                            .into(),
                                    )),
                                ))
                                .builder()
                                .into(),
                            )),
                            ParmElem::TernaryExpression(Box::new(
                                Factor::Intrinsics(Intrinsics::FieldStr(
                                    "CustTable".into(),
                                    "DlvMode".into(),
                                ))
                                .builder()
                                .into(),
                            )),
                            ParmElem::TernaryExpression(Box::new(
                                Factor::Function(Function::QueryValue(
                                    ParmElem::TernaryExpression(Box::new(
                                        Factor::Constant(Constant::String("Baz".into()))
                                            .builder()
                                            .into(),
                                    )),
                                ))
                                .builder()
                                .into(),
                            )),
                        ]))
                        .builder()
                        .into(),
                    ))]),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("queryRun".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::New("QueryRun".to_owned()),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::Id("query".to_owned()))
                                .builder()
                                .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("queryRun".to_owned()),
                        "next".to_owned(),
                    ),
                    parm_list: None,
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("custTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "get".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("custTable".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
            ],
        };

        assert_eq!(Some(expected_ast), actual_ast)
    }

    #[test]
    fn select_firstonly_table_where_conditional_or_and_or_expression() {
        let mut visitor = TranspilerVisitor::new();
        let parse_result = r#"select firstonly custTable
                                where custTable.AccountNum == '4000'
                                    || custTable.Name == 'Foo'
                                    && custTable.DlvMode == 'Baz'
                                    || custTable.Active == NoYes::Yes;"#
            .parse::<SelectStmtParsed>();

        assert!(
            parse_result.is_ok(),
            "Parsing failed: {}",
            parse_result.unwrap_err()
        );
        let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

        assert_eq!(left_over, "");

        ast.take_visitor_enter_leave(&mut visitor);
        let actual_ast = visitor.into_transpiled_ast();

        let expected_ast = LocalBody {
            dcl_list: vec![
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsCustTable".to_owned()
                        },
                        assignment_clause: None,
                    })
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("Query".to_owned()),
                            id: "query".to_owned()
                        },
                        assignment_clause: None,
                    })
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryRun".to_owned()),
                            id: "queryRun".to_owned()
                        },
                        assignment_clause: None,
                    })
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("CustTable".to_owned()),
                            id: "custTable".to_owned()
                        },
                        assignment_clause: None,
                    })
                },
            ],
            stmt_list: vec![
                AssignmentStmt::FieldAssign {
                    field: Field::Id("query".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(
                        Eval {
                            eval_name: EvalName::New("Query".to_owned()),
                            parm_list: None
                        }
                    ).builder().into()
                }.into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsCustTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(
                        Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("query".to_owned()),
                                "addDataSource".to_owned()
                            ),
                            parm_list: Some(
                                vec![
                                    ParmElem::TernaryExpression(
                                        Box::new(Factor::Field(
                                            Field::QualifierId(
                                                Qualifier::Id("custTable".to_owned()),
                                                "TableId".to_owned()
                                            )
                                        ).builder().into())
                                    )
                                ]
                            )
                        }
                    ).builder().into()
                }.into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsCustTable".to_owned()),
                        "firstOnly".to_owned()
                    ),
                    parm_list: Some(vec![
                        ParmElem::TernaryExpression(
                            Box::new(Factor::Constant(
                                Constant::True
                            ).builder().into())
                        )
                    ])
                }.into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Eval(Box::new(Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("qbdsCustTable".to_owned()),
                                "addRange".to_owned()
                            ),
                            parm_list: Some(
                                vec![
                                    ParmElem::TernaryExpression(
                                        Box::new(Factor::Intrinsics(
                                            // this range will have no effect in the end;
                                            // we use DataAreaId here as a convention indicating to the user
                                            // that the range will actually be useles, because it will be
                                            // solely determined by the query range **value**
                                            // see also: https://web.archive.org/web/20190305223625/http://www.axaptapedia.com/Expressions_in_query_ranges
                                            Intrinsics::FieldNum("CustTable".to_owned(), "DataAreaId".to_owned())
                                        ).builder().into())
                                    )
                                ]
                            )
                        })),
                        "value".to_owned()
                    ),
                    parm_list: Some(
                        vec![
                            ParmElem::TernaryExpression(
                                Box::new(Factor::Function(
                                    Function::StrFmt(vec![
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Constant(
                                                    Constant::String(r#"((((%1 == "%2") || (%3 == "%4")) && (%5 == "%6")) || (%7 == %8))"#.into())
                                                ).builder().into()
                                            )
                                        ),
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Intrinsics(
                                                    Intrinsics::FieldStr("CustTable".into(), "AccountNum".into())
                                                ).builder().into()
                                            )
                                        ),
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Function(
                                                    Function::QueryValue(
                                                        ParmElem::TernaryExpression(
                                                            Box::new(
                                                                Factor::Constant(
                                                                    Constant::String("4000".into())
                                                                ).builder().into()
                                                            )
                                                        )
                                                    )
                                                ).builder().into()
                                            )
                                        ),
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Intrinsics(
                                                    Intrinsics::FieldStr("CustTable".into(), "Name".into())
                                                ).builder().into()
                                            )
                                        ),
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Function(
                                                    Function::QueryValue(
                                                        ParmElem::TernaryExpression(
                                                            Box::new(
                                                                Factor::Constant(
                                                                    Constant::String("Foo".into())
                                                                ).builder().into()
                                                            )
                                                        )
                                                    )
                                                ).builder().into()
                                            )
                                        ),
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Intrinsics(
                                                    Intrinsics::FieldStr("CustTable".into(), "DlvMode".into())
                                                ).builder().into()
                                            )
                                        ),
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Function(
                                                    Function::QueryValue(
                                                        ParmElem::TernaryExpression(
                                                            Box::new(
                                                                Factor::Constant(
                                                                    Constant::String("Baz".into())
                                                                ).builder().into()
                                                            )
                                                        )
                                                    )
                                                ).builder().into()
                                            )
                                        ),
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Intrinsics(
                                                    Intrinsics::FieldStr("CustTable".into(), "Active".into())
                                                ).builder().into()
                                            )
                                        ),
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Function(
                                                    Function::QueryValue(
                                                        ParmElem::TernaryExpression(
                                                            Box::new(
                                                                Factor::Constant(
                                                                    Constant::Enum("NoYes".into(), "Yes".into())
                                                                ).builder().into()
                                                            )
                                                        )
                                                    )
                                                ).builder().into()
                                            )
                                        ),
                                    ])
                                ).builder().into())
                            )
                        ]
                    )
                }.into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("queryRun".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(
                        Eval {
                            eval_name: EvalName::New("QueryRun".to_owned()),
                            parm_list: Some(
                                vec![
                                    ParmElem::TernaryExpression(
                                        Box::new(Factor::Field(
                                            Field::Id("query".to_owned())
                                        ).builder().into())
                                    )
                                ]
                            )
                        }
                    ).builder().into()
                }.into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("queryRun".to_owned()),
                        "next".to_owned()
                    ),
                    parm_list: None
                }.into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("custTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(
                        Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("queryRun".to_owned()),
                                "get".to_owned()
                            ),
                            parm_list: Some(
                                vec![
                                    ParmElem::TernaryExpression(Box::new(
                                        Factor::Field(
                                            Field::QualifierId(
                                                Qualifier::Id("custTable".to_owned()),
                                                "TableId".to_owned()
                                            )
                                        ).builder().into()
                                    ))
                                ]
                            )
                        }
                    ).builder().into()
                }.into(),
            ]
        };

        assert_eq!(Some(expected_ast), actual_ast)
    }

    #[test]
    fn select_firstonly_table_where_conditional_or_and_or_parenthesized_expression() {
        let mut visitor = TranspilerVisitor::new();
        let parse_result = r#"select firstonly custTable
                                where custTable.AccountNum == '4000'
                                    ||  (custTable.Name == 'Foo'
                                            && custTable.DlvMode == 'Baz'
                                        )
                                    || custTable.Active == NoYes::Yes;"#
            .parse::<SelectStmtParsed>();

        assert!(
            parse_result.is_ok(),
            "Parsing failed: {}",
            parse_result.unwrap_err()
        );
        let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

        assert_eq!(left_over, "");

        ast.take_visitor_enter_leave(&mut visitor);
        let actual_ast = visitor.into_transpiled_ast();

        let expected_ast = LocalBody {
            dcl_list: vec![
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsCustTable".to_owned()
                        },
                        assignment_clause: None,
                    })
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("Query".to_owned()),
                            id: "query".to_owned()
                        },
                        assignment_clause: None,
                    })
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryRun".to_owned()),
                            id: "queryRun".to_owned()
                        },
                        assignment_clause: None,
                    })
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("CustTable".to_owned()),
                            id: "custTable".to_owned()
                        },
                        assignment_clause: None,
                    })
                },
            ],
            stmt_list: vec![
                AssignmentStmt::FieldAssign {
                    field: Field::Id("query".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(
                        Eval {
                            eval_name: EvalName::New("Query".to_owned()),
                            parm_list: None
                        }
                    ).builder().into()
                }.into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsCustTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(
                        Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("query".to_owned()),
                                "addDataSource".to_owned()
                            ),
                            parm_list: Some(
                                vec![
                                    ParmElem::TernaryExpression(
                                        Box::new(Factor::Field(
                                            Field::QualifierId(
                                                Qualifier::Id("custTable".to_owned()),
                                                "TableId".to_owned()
                                            )
                                        ).builder().into())
                                    )
                                ]
                            )
                        }
                    ).builder().into()
                }.into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsCustTable".to_owned()),
                        "firstOnly".to_owned()
                    ),
                    parm_list: Some(vec![
                        ParmElem::TernaryExpression(
                            Box::new(Factor::Constant(
                                Constant::True
                            ).builder().into())
                        )
                    ])
                }.into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Eval(Box::new(Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("qbdsCustTable".to_owned()),
                                "addRange".to_owned()
                            ),
                            parm_list: Some(
                                vec![
                                    ParmElem::TernaryExpression(
                                        Box::new(Factor::Intrinsics(
                                            // this range will have no effect in the end;
                                            // we use DataAreaId here as a convention indicating to the user
                                            // that the range will actually be useles, because it will be
                                            // solely determined by the query range **value**
                                            // see also: https://web.archive.org/web/20190305223625/http://www.axaptapedia.com/Expressions_in_query_ranges
                                            Intrinsics::FieldNum("CustTable".to_owned(), "DataAreaId".to_owned())
                                        ).builder().into())
                                    )
                                ]
                            )
                        })),
                        "value".to_owned()
                    ),
                    parm_list: Some(
                        vec![
                            ParmElem::TernaryExpression(
                                Box::new(Factor::Function(
                                    Function::StrFmt(vec![
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Constant(
                                                    Constant::String(r#"(((%1 == "%2") || ((%3 == "%4") && (%5 == "%6"))) || (%7 == %8))"#.into())
                                                ).builder().into()
                                            )
                                        ),
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Intrinsics(
                                                    Intrinsics::FieldStr("CustTable".into(), "AccountNum".into())
                                                ).builder().into()
                                            )
                                        ),
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Function(
                                                    Function::QueryValue(
                                                        ParmElem::TernaryExpression(
                                                            Box::new(
                                                                Factor::Constant(
                                                                    Constant::String("4000".into())
                                                                ).builder().into()
                                                            )
                                                        )
                                                    )
                                                ).builder().into()
                                            )
                                        ),
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Intrinsics(
                                                    Intrinsics::FieldStr("CustTable".into(), "Name".into())
                                                ).builder().into()
                                            )
                                        ),
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Function(
                                                    Function::QueryValue(
                                                        ParmElem::TernaryExpression(
                                                            Box::new(
                                                                Factor::Constant(
                                                                    Constant::String("Foo".into())
                                                                ).builder().into()
                                                            )
                                                        )
                                                    )
                                                ).builder().into()
                                            )
                                        ),
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Intrinsics(
                                                    Intrinsics::FieldStr("CustTable".into(), "DlvMode".into())
                                                ).builder().into()
                                            )
                                        ),
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Function(
                                                    Function::QueryValue(
                                                        ParmElem::TernaryExpression(
                                                            Box::new(
                                                                Factor::Constant(
                                                                    Constant::String("Baz".into())
                                                                ).builder().into()
                                                            )
                                                        )
                                                    )
                                                ).builder().into()
                                            )
                                        ),
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Intrinsics(
                                                    Intrinsics::FieldStr("CustTable".into(), "Active".into())
                                                ).builder().into()
                                            )
                                        ),
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Function(
                                                    Function::QueryValue(
                                                        ParmElem::TernaryExpression(
                                                            Box::new(
                                                                Factor::Constant(
                                                                    Constant::Enum("NoYes".into(), "Yes".into())
                                                                ).builder().into()
                                                            )
                                                        )
                                                    )
                                                ).builder().into()
                                            )
                                        ),
                                    ])
                                ).builder().into())
                            )
                        ]
                    )
                }.into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("queryRun".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(
                        Eval {
                            eval_name: EvalName::New("QueryRun".to_owned()),
                            parm_list: Some(
                                vec![
                                    ParmElem::TernaryExpression(
                                        Box::new(Factor::Field(
                                            Field::Id("query".to_owned())
                                        ).builder().into())
                                    )
                                ]
                            )
                        }
                    ).builder().into()
                }.into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("queryRun".to_owned()),
                        "next".to_owned()
                    ),
                    parm_list: None
                }.into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("custTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(
                        Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("queryRun".to_owned()),
                                "get".to_owned()
                            ),
                            parm_list: Some(
                                vec![
                                    ParmElem::TernaryExpression(Box::new(
                                        Factor::Field(
                                            Field::QualifierId(
                                                Qualifier::Id("custTable".to_owned()),
                                                "TableId".to_owned()
                                            )
                                        ).builder().into()
                                    ))
                                ]
                            )
                        }
                    ).builder().into()
                }.into(),
            ]
        };

        assert_eq!(Some(expected_ast), actual_ast)
    }

    #[test]
    fn select_firstonly_table_where_join_where_only_on_table() {
        let mut visitor = TranspilerVisitor::new();
        let parse_result = r#"select firstonly custTable
                                where custTable.AccountNum == '4000'
                                join dirParty
                                    where dirParty.Party == custTable.Party;"#
            .parse::<SelectStmtParsed>();

        assert!(
            parse_result.is_ok(),
            "Parsing failed: {}",
            parse_result.unwrap_err()
        );
        let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

        assert_eq!(left_over, "");

        ast.take_visitor_enter_leave(&mut visitor);
        let actual_ast = visitor.into_transpiled_ast();

        let expected_ast = LocalBody {
            dcl_list: vec![
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsCustTable".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsDirParty".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("Query".to_owned()),
                            id: "query".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryRun".to_owned()),
                            id: "queryRun".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("CustTable".to_owned()),
                            id: "custTable".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("DirParty".to_owned()),
                            id: "dirParty".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
            ],
            stmt_list: vec![
                AssignmentStmt::FieldAssign {
                    field: Field::Id("query".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::New("Query".to_owned()),
                        parm_list: None,
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsCustTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("query".to_owned()),
                            "addDataSource".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("custTable".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsCustTable".to_owned()),
                        "firstOnly".to_owned(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::True).builder().into(),
                    ))]),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Eval(Box::new(Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("qbdsCustTable".to_owned()),
                                "addRange".to_owned(),
                            ),
                            parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                                Factor::Intrinsics(Intrinsics::FieldNum(
                                    "CustTable".to_owned(),
                                    "AccountNum".to_owned(),
                                ))
                                .builder()
                                .into(),
                            ))]),
                        })),
                        "value".to_owned(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::String("4000".to_owned()))
                            .builder()
                            .into(),
                    ))]),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsDirParty".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("qbdsCustTable".to_owned()),
                            "addDataSource".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("dirParty".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsDirParty".to_owned()),
                        "addLink".to_owned(),
                    ),
                    parm_list: Some(vec![
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "CustTable".to_owned(),
                                "Party".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "DirParty".to_owned(),
                                "Party".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                    ]),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("queryRun".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::New("QueryRun".to_owned()),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::Id("query".to_owned()))
                                .builder()
                                .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("queryRun".to_owned()),
                        "next".to_owned(),
                    ),
                    parm_list: None,
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("custTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "get".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("custTable".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("dirParty".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "get".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("dirParty".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
            ],
        };

        assert_eq!(Some(expected_ast), actual_ast)
    }

    #[test]
    fn select_firstonly_table_where_join_where_only_on_table_but_on_two_fields_with_and() {
        let mut visitor = TranspilerVisitor::new();
        let parse_result = r#"select firstonly custTable
                                where custTable.AccountNum == '4000'
                                join dirParty
                                    where dirParty.Party == custTable.Party
                                        && dirParty.PartyName == custTable.CustName;"#
            .parse::<SelectStmtParsed>();

        assert!(
            parse_result.is_ok(),
            "Parsing failed: {}",
            parse_result.unwrap_err()
        );
        let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

        assert_eq!(left_over, "");

        ast.take_visitor_enter_leave(&mut visitor);
        let actual_ast = visitor.into_transpiled_ast();

        let expected_ast = LocalBody {
            dcl_list: vec![
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsCustTable".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsDirParty".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("Query".to_owned()),
                            id: "query".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryRun".to_owned()),
                            id: "queryRun".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("CustTable".to_owned()),
                            id: "custTable".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("DirParty".to_owned()),
                            id: "dirParty".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
            ],
            stmt_list: vec![
                AssignmentStmt::FieldAssign {
                    field: Field::Id("query".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::New("Query".to_owned()),
                        parm_list: None,
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsCustTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("query".to_owned()),
                            "addDataSource".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("custTable".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsCustTable".to_owned()),
                        "firstOnly".to_owned(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::True).builder().into(),
                    ))]),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Eval(Box::new(Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("qbdsCustTable".to_owned()),
                                "addRange".to_owned(),
                            ),
                            parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                                Factor::Intrinsics(Intrinsics::FieldNum(
                                    "CustTable".to_owned(),
                                    "AccountNum".to_owned(),
                                ))
                                .builder()
                                .into(),
                            ))]),
                        })),
                        "value".to_owned(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::String("4000".to_owned()))
                            .builder()
                            .into(),
                    ))]),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsDirParty".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("qbdsCustTable".to_owned()),
                            "addDataSource".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("dirParty".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsDirParty".to_owned()),
                        "addLink".to_owned(),
                    ),
                    parm_list: Some(vec![
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "CustTable".to_owned(),
                                "Party".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "DirParty".to_owned(),
                                "Party".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                    ]),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsDirParty".to_owned()),
                        "addLink".to_owned(),
                    ),
                    parm_list: Some(vec![
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "CustTable".to_owned(),
                                "CustName".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "DirParty".to_owned(),
                                "PartyName".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                    ]),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("queryRun".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::New("QueryRun".to_owned()),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::Id("query".to_owned()))
                                .builder()
                                .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("queryRun".to_owned()),
                        "next".to_owned(),
                    ),
                    parm_list: None,
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("custTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "get".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("custTable".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("dirParty".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "get".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("dirParty".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
            ],
        };

        assert_eq!(Some(expected_ast), actual_ast)
    }

    #[test]
    fn select_firstonly_table_where_join_where_on_table_with_additional_constraint() {
        let mut visitor = TranspilerVisitor::new();
        let parse_result = r#"select firstonly custTable
                                where custTable.AccountNum == '4000'
                                join dirParty
                                    where dirParty.Party == custTable.Party
                                        && dirParty.Active == NoYes::Yes;"#
            .parse::<SelectStmtParsed>();

        assert!(
            parse_result.is_ok(),
            "Parsing failed: {}",
            parse_result.unwrap_err()
        );
        let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

        assert_eq!(left_over, "");

        ast.take_visitor_enter_leave(&mut visitor);
        let actual_ast = visitor.into_transpiled_ast();

        let expected_ast = LocalBody {
            dcl_list: vec![
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsCustTable".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsDirParty".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("Query".to_owned()),
                            id: "query".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryRun".to_owned()),
                            id: "queryRun".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("CustTable".to_owned()),
                            id: "custTable".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("DirParty".to_owned()),
                            id: "dirParty".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
            ],
            stmt_list: vec![
                AssignmentStmt::FieldAssign {
                    field: Field::Id("query".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::New("Query".to_owned()),
                        parm_list: None,
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsCustTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("query".to_owned()),
                            "addDataSource".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("custTable".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsCustTable".to_owned()),
                        "firstOnly".to_owned(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::True).builder().into(),
                    ))]),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Eval(Box::new(Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("qbdsCustTable".to_owned()),
                                "addRange".to_owned(),
                            ),
                            parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                                Factor::Intrinsics(Intrinsics::FieldNum(
                                    "CustTable".to_owned(),
                                    "AccountNum".to_owned(),
                                ))
                                .builder()
                                .into(),
                            ))]),
                        })),
                        "value".to_owned(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::String("4000".to_owned()))
                            .builder()
                            .into(),
                    ))]),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsDirParty".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("qbdsCustTable".to_owned()),
                            "addDataSource".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("dirParty".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsDirParty".to_owned()),
                        "addLink".to_owned(),
                    ),
                    parm_list: Some(vec![
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "CustTable".to_owned(),
                                "Party".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "DirParty".to_owned(),
                                "Party".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                    ]),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Eval(Box::new(Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("qbdsDirParty".to_owned()),
                                "addRange".to_owned(),
                            ),
                            parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                                Factor::Intrinsics(Intrinsics::FieldNum(
                                    "DirParty".to_owned(),
                                    "Active".to_owned(),
                                ))
                                .builder()
                                .into(),
                            ))]),
                        })),
                        "value".to_owned(),
                    ),
                    parm_list: Some(vec![Constant::Enum("NoYes".into(), "Yes".into())
                        .into_parm_elem_for_query_value()]),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("queryRun".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::New("QueryRun".to_owned()),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::Id("query".to_owned()))
                                .builder()
                                .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("queryRun".to_owned()),
                        "next".to_owned(),
                    ),
                    parm_list: None,
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("custTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "get".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("custTable".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("dirParty".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "get".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("dirParty".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
            ],
        };

        assert_eq!(Some(expected_ast), actual_ast)
    }

    #[test]
    fn select_firstonly_table_where_join_with_two_tables_from_grand_parent_to_parent_to_child() {
        let mut visitor = TranspilerVisitor::new();
        let parse_result = r#"select firstonly custTable
                                where custTable.AccountNum == '4000'
                                join dirParty
                                    where dirParty.Party == custTable.Party
                                join logisticsLocation
                                    where logisticsLocation.Address == dirParty.Address;"#
            .parse::<SelectStmtParsed>();

        assert!(
            parse_result.is_ok(),
            "Parsing failed: {}",
            parse_result.unwrap_err()
        );
        let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

        assert_eq!(left_over, "");

        ast.take_visitor_enter_leave(&mut visitor);
        let actual_ast = visitor.into_transpiled_ast();

        let expected_ast = LocalBody {
            dcl_list: vec![
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsCustTable".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsDirParty".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsLogisticsLocation".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("Query".to_owned()),
                            id: "query".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryRun".to_owned()),
                            id: "queryRun".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("CustTable".to_owned()),
                            id: "custTable".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("DirParty".to_owned()),
                            id: "dirParty".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("LogisticsLocation".to_owned()),
                            id: "logisticsLocation".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
            ],
            stmt_list: vec![
                AssignmentStmt::FieldAssign {
                    field: Field::Id("query".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::New("Query".to_owned()),
                        parm_list: None,
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsCustTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("query".to_owned()),
                            "addDataSource".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("custTable".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsCustTable".to_owned()),
                        "firstOnly".to_owned(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::True).builder().into(),
                    ))]),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Eval(Box::new(Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("qbdsCustTable".to_owned()),
                                "addRange".to_owned(),
                            ),
                            parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                                Factor::Intrinsics(Intrinsics::FieldNum(
                                    "CustTable".to_owned(),
                                    "AccountNum".to_owned(),
                                ))
                                .builder()
                                .into(),
                            ))]),
                        })),
                        "value".to_owned(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::String("4000".to_owned()))
                            .builder()
                            .into(),
                    ))]),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsDirParty".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("qbdsCustTable".to_owned()),
                            "addDataSource".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("dirParty".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsDirParty".to_owned()),
                        "addLink".to_owned(),
                    ),
                    parm_list: Some(vec![
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "CustTable".to_owned(),
                                "Party".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "DirParty".to_owned(),
                                "Party".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                    ]),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsLogisticsLocation".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("qbdsDirParty".to_owned()),
                            "addDataSource".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("logisticsLocation".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsLogisticsLocation".to_owned()),
                        "addLink".to_owned(),
                    ),
                    parm_list: Some(vec![
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "DirParty".to_owned(),
                                "Address".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "LogisticsLocation".to_owned(),
                                "Address".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                    ]),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("queryRun".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::New("QueryRun".to_owned()),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::Id("query".to_owned()))
                                .builder()
                                .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("queryRun".to_owned()),
                        "next".to_owned(),
                    ),
                    parm_list: None,
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("custTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "get".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("custTable".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("dirParty".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "get".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("dirParty".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("logisticsLocation".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "get".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("logisticsLocation".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
            ],
        };

        assert_eq!(Some(expected_ast), actual_ast)
    }

    #[test]
    fn select_firstonly_table_where_join_with_two_tables_and_additional_constraints_from_grand_parent_to_parent_to_child(
    ) {
        let mut visitor = TranspilerVisitor::new();
        let parse_result = r#"select firstonly custTable
                                where custTable.AccountNum == '4000'
                                join dirParty
                                    where dirParty.Party == custTable.Party
                                        && dirParty.Active == NoYes::Yes
                                join logisticsLocation
                                    where logisticsLocation.Address == dirParty.Address
                                        && logisticsLocation.Country == "Germany";"#
            .parse::<SelectStmtParsed>();

        assert!(
            parse_result.is_ok(),
            "Parsing failed: {}",
            parse_result.unwrap_err()
        );
        let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

        assert_eq!(left_over, "");

        ast.take_visitor_enter_leave(&mut visitor);
        let actual_ast = visitor.into_transpiled_ast();

        let expected_ast = LocalBody {
            dcl_list: vec![
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsCustTable".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsDirParty".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsLogisticsLocation".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("Query".to_owned()),
                            id: "query".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryRun".to_owned()),
                            id: "queryRun".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("CustTable".to_owned()),
                            id: "custTable".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("DirParty".to_owned()),
                            id: "dirParty".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("LogisticsLocation".to_owned()),
                            id: "logisticsLocation".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
            ],
            stmt_list: vec![
                AssignmentStmt::FieldAssign {
                    field: Field::Id("query".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::New("Query".to_owned()),
                        parm_list: None,
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsCustTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("query".to_owned()),
                            "addDataSource".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("custTable".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsCustTable".to_owned()),
                        "firstOnly".to_owned(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::True).builder().into(),
                    ))]),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Eval(Box::new(Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("qbdsCustTable".to_owned()),
                                "addRange".to_owned(),
                            ),
                            parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                                Factor::Intrinsics(Intrinsics::FieldNum(
                                    "CustTable".to_owned(),
                                    "AccountNum".to_owned(),
                                ))
                                .builder()
                                .into(),
                            ))]),
                        })),
                        "value".to_owned(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::String("4000".to_owned()))
                            .builder()
                            .into(),
                    ))]),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsDirParty".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("qbdsCustTable".to_owned()),
                            "addDataSource".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("dirParty".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsDirParty".to_owned()),
                        "addLink".to_owned(),
                    ),
                    parm_list: Some(vec![
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "CustTable".to_owned(),
                                "Party".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "DirParty".to_owned(),
                                "Party".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                    ]),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Eval(Box::new(Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("qbdsDirParty".to_owned()),
                                "addRange".to_owned(),
                            ),
                            parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                                Factor::Intrinsics(Intrinsics::FieldNum(
                                    "DirParty".to_owned(),
                                    "Active".to_owned(),
                                ))
                                .builder()
                                .into(),
                            ))]),
                        })),
                        "value".to_owned(),
                    ),
                    parm_list: Some(vec![Constant::Enum("NoYes".into(), "Yes".into())
                        .into_parm_elem_for_query_value()]),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsLogisticsLocation".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("qbdsDirParty".to_owned()),
                            "addDataSource".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("logisticsLocation".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsLogisticsLocation".to_owned()),
                        "addLink".to_owned(),
                    ),
                    parm_list: Some(vec![
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "DirParty".to_owned(),
                                "Address".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "LogisticsLocation".to_owned(),
                                "Address".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                    ]),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Eval(Box::new(Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("qbdsLogisticsLocation".to_owned()),
                                "addRange".to_owned(),
                            ),
                            parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                                Factor::Intrinsics(Intrinsics::FieldNum(
                                    "LogisticsLocation".to_owned(),
                                    "Country".to_owned(),
                                ))
                                .builder()
                                .into(),
                            ))]),
                        })),
                        "value".to_owned(),
                    ),
                    parm_list: Some(vec![
                        Constant::String("Germany".into()).into_parm_elem_for_query_value()
                    ]),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("queryRun".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::New("QueryRun".to_owned()),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::Id("query".to_owned()))
                                .builder()
                                .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("queryRun".to_owned()),
                        "next".to_owned(),
                    ),
                    parm_list: None,
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("custTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "get".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("custTable".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("dirParty".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "get".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("dirParty".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("logisticsLocation".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "get".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("logisticsLocation".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
            ],
        };

        assert_eq!(Some(expected_ast), actual_ast)
    }

    #[test]
    fn select_firstonly_table_where_join_with_two_tables_from_parent_to_child_a_to_child_b() {
        let mut visitor = TranspilerVisitor::new();
        let parse_result = r#"select firstonly custTable
                                where custTable.AccountNum == '4000'
                                join dirParty
                                    where dirParty.Party == custTable.Party
                                join logisticsLocation
                                    where logisticsLocation.Address == custTable.Address;"#
            .parse::<SelectStmtParsed>();

        assert!(
            parse_result.is_ok(),
            "Parsing failed: {}",
            parse_result.unwrap_err()
        );
        let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

        assert_eq!(left_over, "");

        ast.take_visitor_enter_leave(&mut visitor);
        let actual_ast = visitor.into_transpiled_ast();

        let expected_ast = LocalBody {
            dcl_list: vec![
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsCustTable".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsDirParty".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsLogisticsLocation".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("Query".to_owned()),
                            id: "query".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryRun".to_owned()),
                            id: "queryRun".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("CustTable".to_owned()),
                            id: "custTable".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("DirParty".to_owned()),
                            id: "dirParty".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("LogisticsLocation".to_owned()),
                            id: "logisticsLocation".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
            ],
            stmt_list: vec![
                AssignmentStmt::FieldAssign {
                    field: Field::Id("query".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::New("Query".to_owned()),
                        parm_list: None,
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsCustTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("query".to_owned()),
                            "addDataSource".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("custTable".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsCustTable".to_owned()),
                        "firstOnly".to_owned(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::True).builder().into(),
                    ))]),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Eval(Box::new(Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("qbdsCustTable".to_owned()),
                                "addRange".to_owned(),
                            ),
                            parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                                Factor::Intrinsics(Intrinsics::FieldNum(
                                    "CustTable".to_owned(),
                                    "AccountNum".to_owned(),
                                ))
                                .builder()
                                .into(),
                            ))]),
                        })),
                        "value".to_owned(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::String("4000".to_owned()))
                            .builder()
                            .into(),
                    ))]),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsDirParty".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("qbdsCustTable".to_owned()),
                            "addDataSource".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("dirParty".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsDirParty".to_owned()),
                        "addLink".to_owned(),
                    ),
                    parm_list: Some(vec![
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "CustTable".to_owned(),
                                "Party".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "DirParty".to_owned(),
                                "Party".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                    ]),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsDirParty".to_owned()),
                        "fetchMode".into(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::Enum("QueryFetchMode".into(), "One2One".into()))
                            .builder()
                            .into(),
                    ))]),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsLogisticsLocation".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("qbdsCustTable".to_owned()),
                            "addDataSource".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("logisticsLocation".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsLogisticsLocation".to_owned()),
                        "addLink".to_owned(),
                    ),
                    parm_list: Some(vec![
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "CustTable".to_owned(),
                                "Address".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "LogisticsLocation".to_owned(),
                                "Address".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                    ]),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsLogisticsLocation".to_owned()),
                        "fetchMode".into(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::Enum("QueryFetchMode".into(), "One2One".into()))
                            .builder()
                            .into(),
                    ))]),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("queryRun".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::New("QueryRun".to_owned()),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::Id("query".to_owned()))
                                .builder()
                                .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("queryRun".to_owned()),
                        "next".to_owned(),
                    ),
                    parm_list: None,
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("custTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "get".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("custTable".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("dirParty".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "get".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("dirParty".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("logisticsLocation".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "get".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("logisticsLocation".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
            ],
        };

        assert_eq!(Some(expected_ast), actual_ast)
    }

    #[test]
    fn select_firstonly_table_where_join_with_two_tables_child_references_grandparent_and_parent() {
        let mut visitor = TranspilerVisitor::new();
        let parse_result = r#"select firstonly custTable
                                where custTable.AccountNum == '4000'
                                join dirParty
                                    where dirParty.Party == custTable.Party
                                join logisticsLocation
                                    where logisticsLocation.Address == custTable.Address
                                        && logisticsLocation.Location == dirParty.LocationRecId;"#
            .parse::<SelectStmtParsed>();

        assert!(
            parse_result.is_ok(),
            "Parsing failed: {}",
            parse_result.unwrap_err()
        );
        let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

        assert_eq!(left_over, "");

        ast.take_visitor_enter_leave(&mut visitor);
        let actual_ast = visitor.into_transpiled_ast();

        let expected_ast = LocalBody {
            dcl_list: vec![
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsCustTable".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsDirParty".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsLogisticsLocation".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("Query".to_owned()),
                            id: "query".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryRun".to_owned()),
                            id: "queryRun".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("CustTable".to_owned()),
                            id: "custTable".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("DirParty".to_owned()),
                            id: "dirParty".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("LogisticsLocation".to_owned()),
                            id: "logisticsLocation".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
            ],
            stmt_list: vec![
                AssignmentStmt::FieldAssign {
                    field: Field::Id("query".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::New("Query".to_owned()),
                        parm_list: None,
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsCustTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("query".to_owned()),
                            "addDataSource".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("custTable".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsCustTable".to_owned()),
                        "firstOnly".to_owned(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::True).builder().into(),
                    ))]),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Eval(Box::new(Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("qbdsCustTable".to_owned()),
                                "addRange".to_owned(),
                            ),
                            parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                                Factor::Intrinsics(Intrinsics::FieldNum(
                                    "CustTable".to_owned(),
                                    "AccountNum".to_owned(),
                                ))
                                .builder()
                                .into(),
                            ))]),
                        })),
                        "value".to_owned(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::String("4000".to_owned()))
                            .builder()
                            .into(),
                    ))]),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsDirParty".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("qbdsCustTable".to_owned()),
                            "addDataSource".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("dirParty".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsDirParty".to_owned()),
                        "addLink".to_owned(),
                    ),
                    parm_list: Some(vec![
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "CustTable".to_owned(),
                                "Party".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "DirParty".to_owned(),
                                "Party".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                    ]),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsLogisticsLocation".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("qbdsDirParty".to_owned()),
                            "addDataSource".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("logisticsLocation".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsLogisticsLocation".to_owned()),
                        "addLink".to_owned(),
                    ),
                    parm_list: Some(vec![
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "CustTable".to_owned(),
                                "Address".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "LogisticsLocation".to_owned(),
                                "Address".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Eval(Eval {
                                eval_name: EvalName::Qualifier(
                                    Qualifier::Id("qbdsCustTable".into()),
                                    "name".into(),
                                ),
                                parm_list: None,
                            })
                            .builder()
                            .into(),
                        )),
                    ]),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsLogisticsLocation".to_owned()),
                        "addLink".to_owned(),
                    ),
                    parm_list: Some(vec![
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "DirParty".to_owned(),
                                "LocationRecId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "LogisticsLocation".to_owned(),
                                "Location".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                    ]),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("queryRun".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::New("QueryRun".to_owned()),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::Id("query".to_owned()))
                                .builder()
                                .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("queryRun".to_owned()),
                        "next".to_owned(),
                    ),
                    parm_list: None,
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("custTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "get".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("custTable".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("dirParty".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "get".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("dirParty".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("logisticsLocation".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "get".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("logisticsLocation".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
            ],
        };

        assert_eq!(Some(expected_ast), actual_ast)
    }

    #[test]
    fn select_firstonly_table_where_join_with_two_tables_and_additional_condition_child_references_grandparent_and_parent(
    ) {
        let mut visitor = TranspilerVisitor::new();
        let parse_result = r#"select firstonly custTable
                                where custTable.AccountNum == '4000'
                                join dirParty
                                    where dirParty.Party == custTable.Party
                                join logisticsLocation
                                    where logisticsLocation.Address == custTable.Address
                                        && logisticsLocation.Location == dirParty.LocationRecId
                                        && logisticsLocation.Name == 'Deutschland';"#
            .parse::<SelectStmtParsed>();

        assert!(
            parse_result.is_ok(),
            "Parsing failed: {}",
            parse_result.unwrap_err()
        );
        let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

        assert_eq!(left_over, "");

        ast.take_visitor_enter_leave(&mut visitor);
        let actual_ast = visitor.into_transpiled_ast();

        let expected_ast = LocalBody {
            dcl_list: vec![
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsCustTable".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsDirParty".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsLogisticsLocation".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("Query".to_owned()),
                            id: "query".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryRun".to_owned()),
                            id: "queryRun".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("CustTable".to_owned()),
                            id: "custTable".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("DirParty".to_owned()),
                            id: "dirParty".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("LogisticsLocation".to_owned()),
                            id: "logisticsLocation".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
            ],
            stmt_list: vec![
                AssignmentStmt::FieldAssign {
                    field: Field::Id("query".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::New("Query".to_owned()),
                        parm_list: None,
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsCustTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("query".to_owned()),
                            "addDataSource".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("custTable".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsCustTable".to_owned()),
                        "firstOnly".to_owned(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::True).builder().into(),
                    ))]),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Eval(Box::new(Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("qbdsCustTable".to_owned()),
                                "addRange".to_owned(),
                            ),
                            parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                                Factor::Intrinsics(Intrinsics::FieldNum(
                                    "CustTable".to_owned(),
                                    "AccountNum".to_owned(),
                                ))
                                .builder()
                                .into(),
                            ))]),
                        })),
                        "value".to_owned(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::String("4000".to_owned()))
                            .builder()
                            .into(),
                    ))]),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsDirParty".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("qbdsCustTable".to_owned()),
                            "addDataSource".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("dirParty".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsDirParty".to_owned()),
                        "addLink".to_owned(),
                    ),
                    parm_list: Some(vec![
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "CustTable".to_owned(),
                                "Party".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "DirParty".to_owned(),
                                "Party".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                    ]),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsLogisticsLocation".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("qbdsDirParty".to_owned()),
                            "addDataSource".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("logisticsLocation".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsLogisticsLocation".to_owned()),
                        "addLink".to_owned(),
                    ),
                    parm_list: Some(vec![
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "CustTable".to_owned(),
                                "Address".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "LogisticsLocation".to_owned(),
                                "Address".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Eval(Eval {
                                eval_name: EvalName::Qualifier(
                                    Qualifier::Id("qbdsCustTable".into()),
                                    "name".into(),
                                ),
                                parm_list: None,
                            })
                            .builder()
                            .into(),
                        )),
                    ]),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsLogisticsLocation".to_owned()),
                        "addLink".to_owned(),
                    ),
                    parm_list: Some(vec![
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "DirParty".to_owned(),
                                "LocationRecId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "LogisticsLocation".to_owned(),
                                "Location".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                    ]),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Eval(Box::new(Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("qbdsLogisticsLocation".to_owned()),
                                "addRange".to_owned(),
                            ),
                            parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                                Factor::Intrinsics(Intrinsics::FieldNum(
                                    "LogisticsLocation".to_owned(),
                                    "Name".to_owned(),
                                ))
                                .builder()
                                .into(),
                            ))]),
                        })),
                        "value".to_owned(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::String("Deutschland".to_owned()))
                            .builder()
                            .into(),
                    ))]),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("queryRun".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::New("QueryRun".to_owned()),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::Id("query".to_owned()))
                                .builder()
                                .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("queryRun".to_owned()),
                        "next".to_owned(),
                    ),
                    parm_list: None,
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("custTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "get".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("custTable".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("dirParty".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "get".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("dirParty".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("logisticsLocation".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "get".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("logisticsLocation".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
            ],
        };

        assert_eq!(Some(expected_ast), actual_ast)
    }

    #[test]
    /// TODO: currently `where-ranges` always go after `addLinks` in the transpiled output
    /// we have to see, whether this is the best behaviour or not
    /// important to note here is that the behaviour of the query is the same for both variants,
    /// so it is merely an astetic aspect
    fn select_firstonly_table_where_join_with_two_tables_and_additional_condition_condition_in_middle_child_references_grandparent_and_parent(
    ) {
        let mut visitor = TranspilerVisitor::new();
        let parse_result = r#"select firstonly custTable
                                where custTable.AccountNum == '4000'
                                join dirParty
                                    where dirParty.Party == custTable.Party
                                join logisticsLocation
                                    where logisticsLocation.Address == custTable.Address
                                        && logisticsLocation.Name == 'Deutschland'
                                        && logisticsLocation.Location == dirParty.LocationRecId;"#
            .parse::<SelectStmtParsed>();

        assert!(
            parse_result.is_ok(),
            "Parsing failed: {}",
            parse_result.unwrap_err()
        );
        let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

        assert_eq!(left_over, "");

        ast.take_visitor_enter_leave(&mut visitor);
        let actual_ast = visitor.into_transpiled_ast();

        let expected_ast = LocalBody {
            dcl_list: vec![
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsCustTable".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsDirParty".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsLogisticsLocation".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("Query".to_owned()),
                            id: "query".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryRun".to_owned()),
                            id: "queryRun".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("CustTable".to_owned()),
                            id: "custTable".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("DirParty".to_owned()),
                            id: "dirParty".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("LogisticsLocation".to_owned()),
                            id: "logisticsLocation".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
            ],
            stmt_list: vec![
                AssignmentStmt::FieldAssign {
                    field: Field::Id("query".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::New("Query".to_owned()),
                        parm_list: None,
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsCustTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("query".to_owned()),
                            "addDataSource".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("custTable".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsCustTable".to_owned()),
                        "firstOnly".to_owned(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::True).builder().into(),
                    ))]),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Eval(Box::new(Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("qbdsCustTable".to_owned()),
                                "addRange".to_owned(),
                            ),
                            parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                                Factor::Intrinsics(Intrinsics::FieldNum(
                                    "CustTable".to_owned(),
                                    "AccountNum".to_owned(),
                                ))
                                .builder()
                                .into(),
                            ))]),
                        })),
                        "value".to_owned(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::String("4000".to_owned()))
                            .builder()
                            .into(),
                    ))]),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsDirParty".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("qbdsCustTable".to_owned()),
                            "addDataSource".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("dirParty".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsDirParty".to_owned()),
                        "addLink".to_owned(),
                    ),
                    parm_list: Some(vec![
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "CustTable".to_owned(),
                                "Party".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "DirParty".to_owned(),
                                "Party".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                    ]),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsLogisticsLocation".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("qbdsDirParty".to_owned()),
                            "addDataSource".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("logisticsLocation".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsLogisticsLocation".to_owned()),
                        "addLink".to_owned(),
                    ),
                    parm_list: Some(vec![
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "CustTable".to_owned(),
                                "Address".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "LogisticsLocation".to_owned(),
                                "Address".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Eval(Eval {
                                eval_name: EvalName::Qualifier(
                                    Qualifier::Id("qbdsCustTable".into()),
                                    "name".into(),
                                ),
                                parm_list: None,
                            })
                            .builder()
                            .into(),
                        )),
                    ]),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsLogisticsLocation".to_owned()),
                        "addLink".to_owned(),
                    ),
                    parm_list: Some(vec![
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "DirParty".to_owned(),
                                "LocationRecId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "LogisticsLocation".to_owned(),
                                "Location".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                    ]),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Eval(Box::new(Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("qbdsLogisticsLocation".to_owned()),
                                "addRange".to_owned(),
                            ),
                            parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                                Factor::Intrinsics(Intrinsics::FieldNum(
                                    "LogisticsLocation".to_owned(),
                                    "Name".to_owned(),
                                ))
                                .builder()
                                .into(),
                            ))]),
                        })),
                        "value".to_owned(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::String("Deutschland".to_owned()))
                            .builder()
                            .into(),
                    ))]),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("queryRun".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::New("QueryRun".to_owned()),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::Id("query".to_owned()))
                                .builder()
                                .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("queryRun".to_owned()),
                        "next".to_owned(),
                    ),
                    parm_list: None,
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("custTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "get".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("custTable".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("dirParty".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "get".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("dirParty".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("logisticsLocation".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "get".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("logisticsLocation".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
            ],
        };

        assert_eq!(Some(expected_ast), actual_ast)
    }

    #[test]
    fn select_firstonly_table_where_join_with_two_tables_and_additional_condition_condition_at_top_child_references_grandparent_and_parent(
    ) {
        let mut visitor = TranspilerVisitor::new();
        let parse_result = r#"select firstonly custTable
                                where custTable.AccountNum == '4000'
                                join dirParty
                                    where dirParty.Party == custTable.Party
                                join logisticsLocation
                                    where logisticsLocation.Name == 'Deutschland'
                                        && logisticsLocation.Address == custTable.Address
                                        && logisticsLocation.Location == dirParty.LocationRecId;"#
            .parse::<SelectStmtParsed>();

        assert!(
            parse_result.is_ok(),
            "Parsing failed: {}",
            parse_result.unwrap_err()
        );
        let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

        assert_eq!(left_over, "");

        ast.take_visitor_enter_leave(&mut visitor);
        let actual_ast = visitor.into_transpiled_ast();

        let expected_ast = LocalBody {
            dcl_list: vec![
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsCustTable".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsDirParty".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsLogisticsLocation".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("Query".to_owned()),
                            id: "query".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryRun".to_owned()),
                            id: "queryRun".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("CustTable".to_owned()),
                            id: "custTable".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("DirParty".to_owned()),
                            id: "dirParty".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("LogisticsLocation".to_owned()),
                            id: "logisticsLocation".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
            ],
            stmt_list: vec![
                AssignmentStmt::FieldAssign {
                    field: Field::Id("query".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::New("Query".to_owned()),
                        parm_list: None,
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsCustTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("query".to_owned()),
                            "addDataSource".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("custTable".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsCustTable".to_owned()),
                        "firstOnly".to_owned(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::True).builder().into(),
                    ))]),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Eval(Box::new(Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("qbdsCustTable".to_owned()),
                                "addRange".to_owned(),
                            ),
                            parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                                Factor::Intrinsics(Intrinsics::FieldNum(
                                    "CustTable".to_owned(),
                                    "AccountNum".to_owned(),
                                ))
                                .builder()
                                .into(),
                            ))]),
                        })),
                        "value".to_owned(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::String("4000".to_owned()))
                            .builder()
                            .into(),
                    ))]),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsDirParty".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("qbdsCustTable".to_owned()),
                            "addDataSource".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("dirParty".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsDirParty".to_owned()),
                        "addLink".to_owned(),
                    ),
                    parm_list: Some(vec![
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "CustTable".to_owned(),
                                "Party".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "DirParty".to_owned(),
                                "Party".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                    ]),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsLogisticsLocation".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("qbdsDirParty".to_owned()),
                            "addDataSource".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("logisticsLocation".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsLogisticsLocation".to_owned()),
                        "addLink".to_owned(),
                    ),
                    parm_list: Some(vec![
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "CustTable".to_owned(),
                                "Address".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "LogisticsLocation".to_owned(),
                                "Address".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Eval(Eval {
                                eval_name: EvalName::Qualifier(
                                    Qualifier::Id("qbdsCustTable".into()),
                                    "name".into(),
                                ),
                                parm_list: None,
                            })
                            .builder()
                            .into(),
                        )),
                    ]),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsLogisticsLocation".to_owned()),
                        "addLink".to_owned(),
                    ),
                    parm_list: Some(vec![
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "DirParty".to_owned(),
                                "LocationRecId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "LogisticsLocation".to_owned(),
                                "Location".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                    ]),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Eval(Box::new(Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("qbdsLogisticsLocation".to_owned()),
                                "addRange".to_owned(),
                            ),
                            parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                                Factor::Intrinsics(Intrinsics::FieldNum(
                                    "LogisticsLocation".to_owned(),
                                    "Name".to_owned(),
                                ))
                                .builder()
                                .into(),
                            ))]),
                        })),
                        "value".to_owned(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::String("Deutschland".to_owned()))
                            .builder()
                            .into(),
                    ))]),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("queryRun".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::New("QueryRun".to_owned()),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::Id("query".to_owned()))
                                .builder()
                                .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("queryRun".to_owned()),
                        "next".to_owned(),
                    ),
                    parm_list: None,
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("custTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "get".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("custTable".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("dirParty".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "get".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("dirParty".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("logisticsLocation".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "get".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("logisticsLocation".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
            ],
        };

        assert_eq!(Some(expected_ast), actual_ast)
    }

    #[test]
    fn select_firstonly_table_where_join_with_two_tables_and_two_additional_conditions_conditions_use_add_range_conditions_at_top_child_references_grandparent_and_parent(
    ) {
        let mut visitor = TranspilerVisitor::new();
        let parse_result = r#"select firstonly custTable
                                where custTable.AccountNum == '4000'
                                join dirParty
                                    where dirParty.Party == custTable.Party
                                join logisticsLocation
                                    where logisticsLocation.Name == 'Deutschland'
                                        && logisticsLocation.Description == 'Foo'
                                        && logisticsLocation.Address == custTable.Address
                                        && logisticsLocation.Location == dirParty.LocationRecId;"#
            .parse::<SelectStmtParsed>();

        assert!(
            parse_result.is_ok(),
            "Parsing failed: {}",
            parse_result.unwrap_err()
        );
        let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

        assert_eq!(left_over, "");

        ast.take_visitor_enter_leave(&mut visitor);
        let actual_ast = visitor.into_transpiled_ast();

        let expected_ast = LocalBody {
            dcl_list: vec![
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsCustTable".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsDirParty".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsLogisticsLocation".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("Query".to_owned()),
                            id: "query".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryRun".to_owned()),
                            id: "queryRun".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("CustTable".to_owned()),
                            id: "custTable".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("DirParty".to_owned()),
                            id: "dirParty".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("LogisticsLocation".to_owned()),
                            id: "logisticsLocation".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
            ],
            stmt_list: vec![
                AssignmentStmt::FieldAssign {
                    field: Field::Id("query".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::New("Query".to_owned()),
                        parm_list: None,
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsCustTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("query".to_owned()),
                            "addDataSource".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("custTable".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsCustTable".to_owned()),
                        "firstOnly".to_owned(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::True).builder().into(),
                    ))]),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Eval(Box::new(Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("qbdsCustTable".to_owned()),
                                "addRange".to_owned(),
                            ),
                            parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                                Factor::Intrinsics(Intrinsics::FieldNum(
                                    "CustTable".to_owned(),
                                    "AccountNum".to_owned(),
                                ))
                                .builder()
                                .into(),
                            ))]),
                        })),
                        "value".to_owned(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::String("4000".to_owned()))
                            .builder()
                            .into(),
                    ))]),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsDirParty".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("qbdsCustTable".to_owned()),
                            "addDataSource".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("dirParty".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsDirParty".to_owned()),
                        "addLink".to_owned(),
                    ),
                    parm_list: Some(vec![
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "CustTable".to_owned(),
                                "Party".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "DirParty".to_owned(),
                                "Party".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                    ]),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsLogisticsLocation".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("qbdsDirParty".to_owned()),
                            "addDataSource".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("logisticsLocation".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsLogisticsLocation".to_owned()),
                        "addLink".to_owned(),
                    ),
                    parm_list: Some(vec![
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "CustTable".to_owned(),
                                "Address".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "LogisticsLocation".to_owned(),
                                "Address".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Eval(Eval {
                                eval_name: EvalName::Qualifier(
                                    Qualifier::Id("qbdsCustTable".into()),
                                    "name".into(),
                                ),
                                parm_list: None,
                            })
                            .builder()
                            .into(),
                        )),
                    ]),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsLogisticsLocation".to_owned()),
                        "addLink".to_owned(),
                    ),
                    parm_list: Some(vec![
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "DirParty".to_owned(),
                                "LocationRecId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "LogisticsLocation".to_owned(),
                                "Location".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                    ]),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Eval(Box::new(Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("qbdsLogisticsLocation".to_owned()),
                                "addRange".to_owned(),
                            ),
                            parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                                Factor::Intrinsics(Intrinsics::FieldNum(
                                    "LogisticsLocation".to_owned(),
                                    "Name".to_owned(),
                                ))
                                .builder()
                                .into(),
                            ))]),
                        })),
                        "value".to_owned(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::String("Deutschland".to_owned()))
                            .builder()
                            .into(),
                    ))]),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Eval(Box::new(Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("qbdsLogisticsLocation".to_owned()),
                                "addRange".to_owned(),
                            ),
                            parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                                Factor::Intrinsics(Intrinsics::FieldNum(
                                    "LogisticsLocation".to_owned(),
                                    "Description".to_owned(),
                                ))
                                .builder()
                                .into(),
                            ))]),
                        })),
                        "value".to_owned(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::String("Foo".to_owned()))
                            .builder()
                            .into(),
                    ))]),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("queryRun".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::New("QueryRun".to_owned()),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::Id("query".to_owned()))
                                .builder()
                                .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("queryRun".to_owned()),
                        "next".to_owned(),
                    ),
                    parm_list: None,
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("custTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "get".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("custTable".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("dirParty".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "get".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("dirParty".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("logisticsLocation".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "get".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("logisticsLocation".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
            ],
        };

        assert_eq!(Some(expected_ast), actual_ast)
    }

    #[test]
    fn select_firstonly_table_where_join_with_three_tables_child_references_grandgrandparent_grandparent_and_parent(
    ) {
        let mut visitor = TranspilerVisitor::new();
        let parse_result = r#"select firstonly custTable
                                where custTable.AccountNum == '4000'
                                join salesTable
                                    where salesTable.CustAccount == custTable.AccountNum
                                join dirParty
                                    where dirParty.Party == salesTable.Party
                                join logisticsLocation
                                    where logisticsLocation.Address == custTable.Address
                                        && logisticsLocation.SalesId == salesTable.SalesId
                                        && logisticsLocation.Location == dirParty.LocationRecId;"#
            .parse::<SelectStmtParsed>();

        assert!(
            parse_result.is_ok(),
            "Parsing failed: {}",
            parse_result.unwrap_err()
        );
        let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

        assert_eq!(left_over, "");

        ast.take_visitor_enter_leave(&mut visitor);
        let actual_ast = visitor.into_transpiled_ast();

        let expected_ast = LocalBody {
            dcl_list: vec![
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsCustTable".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsSalesTable".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsDirParty".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsLogisticsLocation".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("Query".to_owned()),
                            id: "query".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryRun".to_owned()),
                            id: "queryRun".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("CustTable".to_owned()),
                            id: "custTable".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("SalesTable".to_owned()),
                            id: "salesTable".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("DirParty".to_owned()),
                            id: "dirParty".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("LogisticsLocation".to_owned()),
                            id: "logisticsLocation".to_owned(),
                        },
                        assignment_clause: None,
                    }),
                },
            ],
            stmt_list: vec![
                AssignmentStmt::FieldAssign {
                    field: Field::Id("query".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::New("Query".to_owned()),
                        parm_list: None,
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsCustTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("query".to_owned()),
                            "addDataSource".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("custTable".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsCustTable".to_owned()),
                        "firstOnly".to_owned(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::True).builder().into(),
                    ))]),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Eval(Box::new(Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("qbdsCustTable".to_owned()),
                                "addRange".to_owned(),
                            ),
                            parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                                Factor::Intrinsics(Intrinsics::FieldNum(
                                    "CustTable".to_owned(),
                                    "AccountNum".to_owned(),
                                ))
                                .builder()
                                .into(),
                            ))]),
                        })),
                        "value".to_owned(),
                    ),
                    parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                        Factor::Constant(Constant::String("4000".to_owned()))
                            .builder()
                            .into(),
                    ))]),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsSalesTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("qbdsCustTable".to_owned()),
                            "addDataSource".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("salesTable".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsSalesTable".to_owned()),
                        "addLink".to_owned(),
                    ),
                    parm_list: Some(vec![
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "CustTable".to_owned(),
                                "AccountNum".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "SalesTable".to_owned(),
                                "CustAccount".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                    ]),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsDirParty".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("qbdsSalesTable".to_owned()),
                            "addDataSource".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("dirParty".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsDirParty".to_owned()),
                        "addLink".to_owned(),
                    ),
                    parm_list: Some(vec![
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "SalesTable".to_owned(),
                                "Party".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "DirParty".to_owned(),
                                "Party".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                    ]),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsLogisticsLocation".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("qbdsDirParty".to_owned()),
                            "addDataSource".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("logisticsLocation".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsLogisticsLocation".to_owned()),
                        "addLink".to_owned(),
                    ),
                    parm_list: Some(vec![
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "CustTable".to_owned(),
                                "Address".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "LogisticsLocation".to_owned(),
                                "Address".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Eval(Eval {
                                eval_name: EvalName::Qualifier(
                                    Qualifier::Id("qbdsCustTable".into()),
                                    "name".into(),
                                ),
                                parm_list: None,
                            })
                            .builder()
                            .into(),
                        )),
                    ]),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsLogisticsLocation".to_owned()),
                        "addLink".to_owned(),
                    ),
                    parm_list: Some(vec![
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "SalesTable".to_owned(),
                                "SalesId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "LogisticsLocation".to_owned(),
                                "SalesId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Eval(Eval {
                                eval_name: EvalName::Qualifier(
                                    Qualifier::Id("qbdsSalesTable".into()),
                                    "name".into(),
                                ),
                                parm_list: None,
                            })
                            .builder()
                            .into(),
                        )),
                    ]),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsLogisticsLocation".to_owned()),
                        "addLink".to_owned(),
                    ),
                    parm_list: Some(vec![
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "DirParty".to_owned(),
                                "LocationRecId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                        ParmElem::TernaryExpression(Box::new(
                            Factor::Intrinsics(Intrinsics::FieldNum(
                                "LogisticsLocation".to_owned(),
                                "Location".to_owned(),
                            ))
                            .builder()
                            .into(),
                        )),
                    ]),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("queryRun".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::New("QueryRun".to_owned()),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::Id("query".to_owned()))
                                .builder()
                                .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("queryRun".to_owned()),
                        "next".to_owned(),
                    ),
                    parm_list: None,
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("custTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "get".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("custTable".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("salesTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "get".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("salesTable".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("dirParty".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "get".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("dirParty".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("logisticsLocation".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "get".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("logisticsLocation".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
            ],
        };

        assert_eq!(Some(expected_ast), actual_ast)
    }

    #[test]
    fn select_firstonly_table_where_join_parenthesized_expression() {
        let mut visitor = TranspilerVisitor::new();
        let parse_result = r#"select firstonly custTable
                                join dirPartyTable
                                    where dirPartyTable.RecId == custTable.Party
                                        && ((dirPartyTable.Type == PartyType::Person
                                                && custTable.AccountNum == '42')
                                            || (dirPartyTable.Type == PartyType::Organization
                                                && custTable.AccountNum == '1337'));"#
            .parse::<SelectStmtParsed>();

        assert!(
            parse_result.is_ok(),
            "Parsing failed: {}",
            parse_result.unwrap_err()
        );
        let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

        assert_eq!(left_over, "");

        ast.take_visitor_enter_leave(&mut visitor);
        let actual_ast = visitor.into_transpiled_ast();

        let expected_ast = LocalBody {
            dcl_list: vec![
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsCustTable".to_owned()
                        },
                        assignment_clause: None,
                    })
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsDirPartyTable".to_owned()
                        },
                        assignment_clause: None,
                    })
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("Query".to_owned()),
                            id: "query".to_owned()
                        },
                        assignment_clause: None,
                    })
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryRun".to_owned()),
                            id: "queryRun".to_owned()
                        },
                        assignment_clause: None,
                    })
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("CustTable".to_owned()),
                            id: "custTable".to_owned()
                        },
                        assignment_clause: None,
                    })
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("DirPartyTable".to_owned()),
                            id: "dirPartyTable".to_owned()
                        },
                        assignment_clause: None,
                    })
                },
            ],
            stmt_list: vec![
                AssignmentStmt::FieldAssign {
                    field: Field::Id("query".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(
                        Eval {
                            eval_name: EvalName::New("Query".to_owned()),
                            parm_list: None
                        }
                    ).builder().into()
                }.into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsCustTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(
                        Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("query".to_owned()),
                                "addDataSource".to_owned()
                            ),
                            parm_list: Some(
                                vec![
                                    ParmElem::TernaryExpression(
                                        Box::new(Factor::Field(
                                            Field::QualifierId(
                                                Qualifier::Id("custTable".to_owned()),
                                                "TableId".to_owned()
                                            )
                                        ).builder().into())
                                    )
                                ]
                            )
                        }
                    ).builder().into()
                }.into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsCustTable".to_owned()),
                        "firstOnly".to_owned()
                    ),
                    parm_list: Some(vec![
                        ParmElem::TernaryExpression(
                            Box::new(Factor::Constant(
                                Constant::True
                            ).builder().into())
                        )
                    ])
                }.into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsDirPartyTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(
                        Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("qbdsCustTable".to_owned()),
                                "addDataSource".to_owned()
                            ),
                            parm_list: Some(
                                vec![
                                    ParmElem::TernaryExpression(
                                        Box::new(Factor::Field(
                                            Field::QualifierId(
                                                Qualifier::Id("dirPartyTable".to_owned()),
                                                "TableId".to_owned()
                                            )
                                        ).builder().into())
                                    )
                                ]
                            )
                        }
                    ).builder().into()
                }.into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsDirPartyTable".to_owned()),
                        "addLink".to_owned()
                    ),
                    parm_list: Some(
                        vec![
                            ParmElem::TernaryExpression(
                                Box::new(Factor::Intrinsics(
                                    Intrinsics::FieldNum("CustTable".to_owned(), "Party".to_owned())
                                ).builder().into())
                            ),
                            ParmElem::TernaryExpression(
                                Box::new(Factor::Intrinsics(
                                    Intrinsics::FieldNum("DirPartyTable".to_owned(), "RecId".to_owned())
                                ).builder().into())
                            )
                        ]
                    )
                }.into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Eval(Box::new(Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("qbdsDirPartyTable".to_owned()),
                                "addRange".to_owned()
                            ),
                            parm_list: Some(
                                vec![
                                    ParmElem::TernaryExpression(
                                        Box::new(Factor::Intrinsics(
                                            // this range will have no effect in the end;
                                            // we use DataAreaId here as a convention indicating to the user
                                            // that the range will actually be useles, because it will be
                                            // solely determined by the query range **value**
                                            // see also: https://web.archive.org/web/20190305223625/http://www.axaptapedia.com/Expressions_in_query_ranges
                                            Intrinsics::FieldNum("DirPartyTable".to_owned(), "DataAreaId".to_owned())
                                        ).builder().into())
                                    )
                                ]
                            )
                        })),
                        "value".to_owned()
                    ),
                    parm_list: Some(
                        vec![
                            ParmElem::TernaryExpression(
                                Box::new(Factor::Function(
                                    Function::StrFmt(vec![
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Constant(
                                                    Constant::String(r#"(((%1 == %2) && (%3 == "%4")) || ((%5 == %6) && (%7 == "%8")))"#.into())
                                                ).builder().into()
                                            )
                                        ),
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Intrinsics(
                                                    Intrinsics::FieldStr("DirPartyTable".into(), "Type".into())
                                                ).builder().into()
                                            )
                                        ),
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Function(
                                                    Function::QueryValue(
                                                        ParmElem::TernaryExpression(
                                                            Box::new(
                                                                Factor::Constant(
                                                                    Constant::Enum("PartyType".into(), "Person".into())
                                                                ).builder().into()
                                                            )
                                                        )
                                                    )
                                                ).builder().into()
                                            )
                                        ),
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Intrinsics(
                                                    Intrinsics::FieldStr("CustTable".into(), "AccountNum".into())
                                                ).builder().into()
                                            )
                                        ),
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Function(
                                                    Function::QueryValue(
                                                        ParmElem::TernaryExpression(
                                                            Box::new(
                                                                Factor::Constant(
                                                                    Constant::String("42".into())
                                                                ).builder().into()
                                                            )
                                                        )
                                                    )
                                                ).builder().into()
                                            )
                                        ),
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Intrinsics(
                                                    Intrinsics::FieldStr("DirPartyTable".into(), "Type".into())
                                                ).builder().into()
                                            )
                                        ),
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Function(
                                                    Function::QueryValue(
                                                        ParmElem::TernaryExpression(
                                                            Box::new(
                                                                Factor::Constant(
                                                                    Constant::Enum("PartyType".into(), "Organization".into())
                                                                ).builder().into()
                                                            )
                                                        )
                                                    )
                                                ).builder().into()
                                            )
                                        ),
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Intrinsics(
                                                    Intrinsics::FieldStr("CustTable".into(), "AccountNum".into())
                                                ).builder().into()
                                            )
                                        ),
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Function(
                                                    Function::QueryValue(
                                                        ParmElem::TernaryExpression(
                                                            Box::new(
                                                                Factor::Constant(
                                                                    Constant::String("1337".into())
                                                                ).builder().into()
                                                            )
                                                        )
                                                    )
                                                ).builder().into()
                                            )
                                        ),
                                    ]
                                )
                            ).builder().into()
                        )
                    )
                        ]
                    )
                }.into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("queryRun".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::New("QueryRun".to_owned()),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::Id("query".to_owned()))
                                .builder()
                                .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("queryRun".to_owned()),
                        "next".to_owned(),
                    ),
                    parm_list: None,
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("custTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "get".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("custTable".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("dirPartyTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "get".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("dirPartyTable".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
            ]
        };

        assert_eq!(Some(expected_ast), actual_ast)
    }

    #[test]
    fn select_firstonly_table_multiple_where_join_parenthesized_expression() {
        let mut visitor = TranspilerVisitor::new();
        let parse_result = r#"select firstonly custTable
                                join dirPartyTable
                                    where dirPartyTable.RecId == custTable.Party
                                        && ((dirPartyTable.Type == PartyType::Person
                                                && custTable.AccountNum == '42')
                                            || (dirPartyTable.Type == PartyType::Organization
                                                && custTable.AccountNum == '1337'))
                                join logisticsLocation
                                    where logisticsLocation.RecId == dirPartyTable.Location
                                        && ((logisticsLocation.Type == AddressType::Postal
                                                && logisticsLocation.Role == RoleType::Business)
                                            || (logisticsLocation.Type == AddressType::Email
                                                && logisticsLocation.Role == RoleType::Invoice));"#
            .parse::<SelectStmtParsed>();

        assert!(
            parse_result.is_ok(),
            "Parsing failed: {}",
            parse_result.unwrap_err()
        );
        let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

        assert_eq!(left_over, "");

        ast.take_visitor_enter_leave(&mut visitor);
        let actual_ast = visitor.into_transpiled_ast();

        let expected_ast = LocalBody {
            dcl_list: vec![
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsCustTable".to_owned()
                        },
                        assignment_clause: None,
                    })
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsDirPartyTable".to_owned()
                        },
                        assignment_clause: None,
                    })
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryBuildDataSource".to_owned()),
                            id: "qbdsLogisticsLocation".to_owned()
                        },
                        assignment_clause: None,
                    })
               },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("Query".to_owned()),
                            id: "query".to_owned()
                        },
                        assignment_clause: None,
                    })
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("QueryRun".to_owned()),
                            id: "queryRun".to_owned()
                        },
                        assignment_clause: None,
                    })
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("CustTable".to_owned()),
                            id: "custTable".to_owned()
                        },
                        assignment_clause: None,
                    })
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("DirPartyTable".to_owned()),
                            id: "dirPartyTable".to_owned()
                        },
                        assignment_clause: None,
                    })
                },
                DeclarationStmt {
                    decl_init_list: DeclarationInitList::DeclarationInit(DeclarationInit {
                        declaration: Declaration {
                            decl_type: DeclarationType::TypeId("LogisticsLocation".to_owned()),
                            id: "logisticsLocation".to_owned()
                        },
                        assignment_clause: None,
                    })
                },
            ],
            stmt_list: vec![
                AssignmentStmt::FieldAssign {
                    field: Field::Id("query".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(
                        Eval {
                            eval_name: EvalName::New("Query".to_owned()),
                            parm_list: None
                        }
                    ).builder().into()
                }.into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsCustTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(
                        Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("query".to_owned()),
                                "addDataSource".to_owned()
                            ),
                            parm_list: Some(
                                vec![
                                    ParmElem::TernaryExpression(
                                        Box::new(Factor::Field(
                                            Field::QualifierId(
                                                Qualifier::Id("custTable".to_owned()),
                                                "TableId".to_owned()
                                            )
                                        ).builder().into())
                                    )
                                ]
                            )
                        }
                    ).builder().into()
                }.into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsCustTable".to_owned()),
                        "firstOnly".to_owned()
                    ),
                    parm_list: Some(vec![
                        ParmElem::TernaryExpression(
                            Box::new(Factor::Constant(
                                Constant::True
                            ).builder().into())
                        )
                    ])
                }.into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsDirPartyTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(
                        Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("qbdsCustTable".to_owned()),
                                "addDataSource".to_owned()
                            ),
                            parm_list: Some(
                                vec![
                                    ParmElem::TernaryExpression(
                                        Box::new(Factor::Field(
                                            Field::QualifierId(
                                                Qualifier::Id("dirPartyTable".to_owned()),
                                                "TableId".to_owned()
                                            )
                                        ).builder().into())
                                    )
                                ]
                            )
                        }
                    ).builder().into()
                }.into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsDirPartyTable".to_owned()),
                        "addLink".to_owned()
                    ),
                    parm_list: Some(
                        vec![
                            ParmElem::TernaryExpression(
                                Box::new(Factor::Intrinsics(
                                    Intrinsics::FieldNum("CustTable".to_owned(), "Party".to_owned())
                                ).builder().into())
                            ),
                            ParmElem::TernaryExpression(
                                Box::new(Factor::Intrinsics(
                                    Intrinsics::FieldNum("DirPartyTable".to_owned(), "RecId".to_owned())
                                ).builder().into())
                            )
                        ]
                    )
                }.into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Eval(Box::new(Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("qbdsDirPartyTable".to_owned()),
                                "addRange".to_owned()
                            ),
                            parm_list: Some(
                                vec![
                                    ParmElem::TernaryExpression(
                                        Box::new(Factor::Intrinsics(
                                            // this range will have no effect in the end;
                                            // we use DataAreaId here as a convention indicating to the user
                                            // that the range will actually be useles, because it will be
                                            // solely determined by the query range **value**
                                            // see also: https://web.archive.org/web/20190305223625/http://www.axaptapedia.com/Expressions_in_query_ranges
                                            Intrinsics::FieldNum("DirPartyTable".to_owned(), "DataAreaId".to_owned())
                                        ).builder().into())
                                    )
                                ]
                            )
                        })),
                        "value".to_owned()
                    ),
                    parm_list: Some(
                        vec![
                            ParmElem::TernaryExpression(
                                Box::new(Factor::Function(
                                    Function::StrFmt(vec![
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Constant(
                                                    Constant::String(r#"(((%1 == %2) && (%3 == "%4")) || ((%5 == %6) && (%7 == "%8")))"#.into())
                                                ).builder().into()
                                            )
                                        ),
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Intrinsics(
                                                    Intrinsics::FieldStr("DirPartyTable".into(), "Type".into())
                                                ).builder().into()
                                            )
                                        ),
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Function(
                                                    Function::QueryValue(
                                                        ParmElem::TernaryExpression(
                                                            Box::new(
                                                                Factor::Constant(
                                                                    Constant::Enum("PartyType".into(), "Person".into())
                                                                ).builder().into()
                                                            )
                                                        )
                                                    )
                                                ).builder().into()
                                            )
                                        ),
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Intrinsics(
                                                    Intrinsics::FieldStr("CustTable".into(), "AccountNum".into())
                                                ).builder().into()
                                            )
                                        ),
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Function(
                                                    Function::QueryValue(
                                                        ParmElem::TernaryExpression(
                                                            Box::new(
                                                                Factor::Constant(
                                                                    Constant::String("42".into())
                                                                ).builder().into()
                                                            )
                                                        )
                                                    )
                                                ).builder().into()
                                            )
                                        ),
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Intrinsics(
                                                    Intrinsics::FieldStr("DirPartyTable".into(), "Type".into())
                                                ).builder().into()
                                            )
                                        ),
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Function(
                                                    Function::QueryValue(
                                                        ParmElem::TernaryExpression(
                                                            Box::new(
                                                                Factor::Constant(
                                                                    Constant::Enum("PartyType".into(), "Organization".into())
                                                                ).builder().into()
                                                            )
                                                        )
                                                    )
                                                ).builder().into()
                                            )
                                        ),
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Intrinsics(
                                                    Intrinsics::FieldStr("CustTable".into(), "AccountNum".into())
                                                ).builder().into()
                                            )
                                        ),
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Function(
                                                    Function::QueryValue(
                                                        ParmElem::TernaryExpression(
                                                            Box::new(
                                                                Factor::Constant(
                                                                    Constant::String("1337".into())
                                                                ).builder().into()
                                                            )
                                                        )
                                                    )
                                                ).builder().into()
                                            )
                                        ),
                                    ]
                                )
                            ).builder().into()
                        )
                    )
                        ]
                    )
                }.into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("qbdsLogisticsLocation".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(
                        Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("qbdsDirPartyTable".to_owned()),
                                "addDataSource".to_owned()
                            ),
                            parm_list: Some(
                                vec![
                                    ParmElem::TernaryExpression(
                                        Box::new(Factor::Field(
                                            Field::QualifierId(
                                                Qualifier::Id("logisticsLocation".to_owned()),
                                                "TableId".to_owned()
                                            )
                                        ).builder().into())
                                    )
                                ]
                            )
                        }
                    ).builder().into()
                }.into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("qbdsLogisticsLocation".to_owned()),
                        "addLink".to_owned()
                    ),
                    parm_list: Some(
                        vec![
                            ParmElem::TernaryExpression(
                                Box::new(Factor::Intrinsics(
                                    Intrinsics::FieldNum("DirPartyTable".to_owned(), "Location".to_owned())
                                ).builder().into())
                            ),
                            ParmElem::TernaryExpression(
                                Box::new(Factor::Intrinsics(
                                    Intrinsics::FieldNum("LogisticsLocation".to_owned(), "RecId".to_owned())
                                ).builder().into())
                            )
                        ]
                    )
                }.into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Eval(Box::new(Eval {
                            eval_name: EvalName::Qualifier(
                                Qualifier::Id("qbdsLogisticsLocation".to_owned()),
                                "addRange".to_owned()
                            ),
                            parm_list: Some(
                                vec![
                                    ParmElem::TernaryExpression(
                                        Box::new(Factor::Intrinsics(
                                            // this range will have no effect in the end;
                                            // we use DataAreaId here as a convention indicating to the user
                                            // that the range will actually be useles, because it will be
                                            // solely determined by the query range **value**
                                            // see also: https://web.archive.org/web/20190305223625/http://www.axaptapedia.com/Expressions_in_query_ranges
                                            Intrinsics::FieldNum("LogisticsLocation".to_owned(), "DataAreaId".to_owned())
                                        ).builder().into())
                                    )
                                ]
                            )
                        })),
                        "value".to_owned()
                    ),
                    parm_list: Some(
                        vec![
                            ParmElem::TernaryExpression(
                                Box::new(Factor::Function(
                                    Function::StrFmt(vec![
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Constant(
                                                    Constant::String(r#"(((%1 == %2) && (%3 == %4)) || ((%5 == %6) && (%7 == %8)))"#.into())
                                                ).builder().into()
                                            )
                                        ),
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Intrinsics(
                                                    Intrinsics::FieldStr("LogisticsLocation".into(), "Type".into())
                                                ).builder().into()
                                            )
                                        ),
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Function(
                                                    Function::QueryValue(
                                                        ParmElem::TernaryExpression(
                                                            Box::new(
                                                                Factor::Constant(
                                                                    Constant::Enum("AddressType".into(), "Postal".into())
                                                                ).builder().into()
                                                            )
                                                        )
                                                    )
                                                ).builder().into()
                                            )
                                        ),
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Intrinsics(
                                                    Intrinsics::FieldStr("LogisticsLocation".into(), "Role".into())
                                                ).builder().into()
                                            )
                                        ),
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Function(
                                                    Function::QueryValue(
                                                        ParmElem::TernaryExpression(
                                                            Box::new(
                                                                Factor::Constant(
                                                                    Constant::Enum("RoleType".into(), "Business".into())
                                                                ).builder().into()
                                                            )
                                                        )
                                                    )
                                                ).builder().into()
                                            )
                                        ),
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Intrinsics(
                                                    Intrinsics::FieldStr("LogisticsLocation".into(), "Type".into())
                                                ).builder().into()
                                            )
                                        ),
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Function(
                                                    Function::QueryValue(
                                                        ParmElem::TernaryExpression(
                                                            Box::new(
                                                                Factor::Constant(
                                                                    Constant::Enum("AddressType".into(), "Email".into())
                                                                ).builder().into()
                                                            )
                                                        )
                                                    )
                                                ).builder().into()
                                            )
                                        ),
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Intrinsics(
                                                    Intrinsics::FieldStr("LogisticsLocation".into(), "Role".into())
                                                ).builder().into()
                                            )
                                        ),
                                        ParmElem::TernaryExpression(
                                            Box::new(
                                                Factor::Function(
                                                    Function::QueryValue(
                                                        ParmElem::TernaryExpression(
                                                            Box::new(
                                                                Factor::Constant(
                                                                    Constant::Enum("RoleType".into(), "Invoice".into())
                                                                ).builder().into()
                                                            )
                                                        )
                                                    )
                                                ).builder().into()
                                            )
                                        ),
                                    ]
                                )
                            ).builder().into()
                        )
                    )
                        ]
                    )
                }.into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("queryRun".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::New("QueryRun".to_owned()),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::Id("query".to_owned()))
                                .builder()
                                .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                Eval {
                    eval_name: EvalName::Qualifier(
                        Qualifier::Id("queryRun".to_owned()),
                        "next".to_owned(),
                    ),
                    parm_list: None,
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("custTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "get".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("custTable".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("dirPartyTable".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "get".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("dirPartyTable".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
                AssignmentStmt::FieldAssign {
                    field: Field::Id("logisticsLocation".to_owned()),
                    assign: Assign::AssignEqual,
                    tern_expr: Factor::Eval(Eval {
                        eval_name: EvalName::Qualifier(
                            Qualifier::Id("queryRun".to_owned()),
                            "get".to_owned(),
                        ),
                        parm_list: Some(vec![ParmElem::TernaryExpression(Box::new(
                            Factor::Field(Field::QualifierId(
                                Qualifier::Id("logisticsLocation".to_owned()),
                                "TableId".to_owned(),
                            ))
                            .builder()
                            .into(),
                        ))]),
                    })
                    .builder()
                    .into(),
                }
                .into(),
            ]
        };

        assert_eq!(Some(expected_ast), actual_ast)
    }
}
