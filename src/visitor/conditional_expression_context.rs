use dracaena::ast::column::Column;
use dracaena::ast::column::FunctionExpression;
use dracaena::ast::eval::Eval;
use dracaena::ast::eval_name::EvalName;
use dracaena::ast::expression::IfExpression;
use dracaena::ast::field::Field;
use dracaena::ast::field_column::FieldList;
use dracaena::ast::index_hint::IndexHint;
use dracaena::ast::intrinsics::Intrinsics;
use dracaena::ast::join::JoinClause;
use dracaena::ast::join::JoinOrder;
use dracaena::ast::join::JoinSpec;
use dracaena::ast::join::JoinUsing;
use dracaena::ast::join::JoinVariant;
use dracaena::ast::operator::ComparisonOperator;
use dracaena::ast::operator::LogicalOperator;
use dracaena::ast::order_elem::Direction;
use dracaena::ast::order_elem::OrderElem;
use dracaena::ast::order_group::GroupOrderBy;
use dracaena::ast::order_group::OrderGroup;
use dracaena::ast::order_group::OrderGroupData;
use dracaena::ast::qualifier::Qualifier;
use dracaena::ast::select_option::FirstOnly;
use dracaena::ast::select_option::ForcePlaceholderLiteral;
use dracaena::ast::select_option::SelectOpts;
use dracaena::ast::table::Table;
use dracaena::ast::table::TableWithFields;
use dracaena::ast::term::TermData;
use dracaena::ast::{
    constant::Constant,
    expression::{
        AsExpression, ComparisonExpression, ConditionalExpression, ConditionalExpressionData,
        Expression, IsExpression, SimpleExpression, SimpleExpressionData, TernaryExpression,
    },
    factor::*,
    find::*,
    from::*,
    function::Function,
    parm::ParmElem,
    term::Term,
};
use dracaena::visitor::visitable::Visitor;
use dracaena::visitor::visitable::VisitorEnterLeave;
use indexmap::IndexMap;

use std::collections::HashMap;

use std::iter;

#[derive(Debug, PartialEq)]
pub(crate) enum ConditionalExpressionContextResolveStrategy {
    AddRange,
    StrFmt,
}

type ParenthesizeLevel = usize;
pub(crate) type ConditionalExpressionDataLevel = usize;
type LogOpIndex = usize;

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
struct TableField<'a> {
    table_name: &'a str,
    field_name: &'a str,
}

impl<'a> TableField<'a> {
    pub fn new(table_name: &'a str, field_name: &'a str) -> Self {
        Self {
            table_name,
            field_name,
        }
    }
}

#[derive(Debug, PartialEq, Clone)]
struct LogOpParensLevel<'a> {
    log_op: &'a LogicalOperator,
    parenthesize_level: ParenthesizeLevel,
}

impl<'a> LogOpParensLevel<'a> {
    pub fn new(log_op: &'a LogicalOperator, parenthesize_level: ParenthesizeLevel) -> Self {
        Self {
            log_op,
            parenthesize_level,
        }
    }
}

#[derive(Debug, PartialEq)]
pub(crate) struct ConditionalExpressionContextVisitor<'a> {
    pub(crate) cond_expr_ctx_resolve_strategy: Option<ConditionalExpressionContextResolveStrategy>,
    current_log_op_idx: LogOpIndex,
    current_parenthesize_level: ParenthesizeLevel,
    current_cond_expr_data_level: ConditionalExpressionDataLevel,
    log_op_idx_to_log_op_parens_level: HashMap<LogOpIndex, LogOpParensLevel<'a>>,
    table_name_field_name_to_log_ops: IndexMap<TableField<'a>, Vec<&'a LogicalOperator>>,
    last_log_op: Option<&'a LogicalOperator>,
}

impl<'a> ConditionalExpressionContextVisitor<'a> {
    pub(crate) fn new() -> Self {
        Self {
            cond_expr_ctx_resolve_strategy: None,
            current_log_op_idx: 0,
            current_parenthesize_level: 0,
            current_cond_expr_data_level: 0,
            log_op_idx_to_log_op_parens_level: HashMap::new(),
            table_name_field_name_to_log_ops: IndexMap::new(),
            last_log_op: None,
        }
    }
}

impl<'a> Visitor<'a> for ConditionalExpressionContextVisitor<'a> {
    fn visit_find_join(&mut self, _node: &'a FindJoin) {}

    fn visit_find_where(&mut self, _node: &'a FindWhere) {}

    fn visit_find_order(&mut self, _node: &'a FindOrder) {}

    fn visit_find_using(&mut self, _node: &'a FindUsing) {}

    fn visit_find_table(&mut self, _node: &'a FindTable) {}

    fn visit_select_table(&mut self, _node: &'a SelectTable) {}

    fn visit_select_stmt(&mut self, _node: &'a SelectStmt) {}

    fn visit_select_opts(&mut self, _node: &'a SelectOpts) {}

    fn visit_table(&mut self, _node: &'a Table) {}

    fn visit_from(&mut self, _node: &'a From) {}

    fn visit_index_hint(&mut self, _node: &'a IndexHint) {}

    fn visit_table_with_fields(&mut self, _node: &'a TableWithFields) {}

    fn visit_join_spec(&mut self, _node: &'a JoinSpec) {}

    fn visit_join_variant(&mut self, _node: &'a JoinVariant) {}

    fn visit_join_clause(&mut self, _node: &'a JoinClause) {}

    fn visit_join_using(&mut self, _node: &'a JoinUsing) {}

    fn visit_join_order(&mut self, _node: &'a JoinOrder) {}

    fn visit_order_group_data(&mut self, _node: &'a OrderGroupData) {}

    fn visit_order_group(&mut self, _node: &'a OrderGroup) {}

    fn visit_group_order_by(&mut self, _node: &'a GroupOrderBy) {}

    fn visit_order_elem(&mut self, _node: &'a OrderElem) {}

    fn visit_direction(&mut self, _node: &'a Direction) {}

    fn visit_first_only(&mut self, _node: &'a FirstOnly) {}

    fn visit_force_placeholder_literal(&mut self, _node: &'a ForcePlaceholderLiteral) {}

    fn visit_function_expr(&mut self, _node: &'a FunctionExpression) {}

    fn visit_column(&mut self, _node: &'a Column) {}

    fn visit_conditional_expr(&mut self, _node: &'a ConditionalExpression) {}

    fn visit_conditional_expr_data(&mut self, _node: &'a ConditionalExpressionData) {}

    fn visit_expr(&mut self, _node: &'a Expression) {}

    fn visit_comparison_expr(&mut self, _node: &'a ComparisonExpression) {}

    fn visit_as_expr(&mut self, _node: &'a AsExpression) {}

    fn visit_is_expr(&mut self, _node: &'a IsExpression) {}

    fn visit_simple_expr(&mut self, _node: &'a SimpleExpression) {}

    fn visit_simple_expr_data(&mut self, _node: &'a SimpleExpressionData) {}

    fn visit_term(&mut self, _node: &'a Term) {}

    fn visit_term_data(&mut self, _node: &'a TermData) {}

    fn visit_complement_factor(&mut self, _node: &'a ComplementFactor) {}

    fn visit_second_factor(&mut self, _node: &'a SecondFactor) {}

    fn visit_factor(&mut self, _node: &'a Factor) {}

    fn visit_constant(&mut self, _node: &'a Constant) {}

    fn visit_field(&mut self, _node: &'a Field) {}

    fn visit_function(&mut self, _node: &'a Function) {}

    fn visit_parm_elem(&mut self, _node: &'a ParmElem) {}

    fn visit_intrinsics(&mut self, _node: &'a Intrinsics) {}

    fn visit_eval(&mut self, _node: &'a Eval) {}

    fn visit_eval_name(&mut self, _node: &'a EvalName) {}

    fn visit_qualifier(&mut self, _node: &'a Qualifier) {}

    fn visit_if_expr(&mut self, _node: &'a IfExpression) {}

    fn visit_ternary_expr(&mut self, _node: &'a TernaryExpression) {}

    fn visit_logical_op(&mut self, node: &'a LogicalOperator) {
        self.log_op_idx_to_log_op_parens_level.insert(
            self.current_log_op_idx,
            LogOpParensLevel::new(node, self.current_parenthesize_level),
        );
        self.last_log_op = Some(node);

        if self.table_name_field_name_to_log_ops.len() == 1 && self.current_log_op_idx == 0 {
            // when we only have one element in our map and we are at the first logical operator,
            // we still have to push this logical operator to the list;
            // in other cases, this map gets filled elsewhere
            if let Some((_, log_ops)) = self.table_name_field_name_to_log_ops.get_index_mut(0) {
                log_ops.push(node);
            }
        }
    }

    fn visit_comparison_op(&mut self, _node: &'a ComparisonOperator) {}

    fn visit_field_list(&mut self, _node: &'a FieldList) {}

    fn visit_dcl_stmt(&mut self, _: &'a dracaena::ast::declaration::DeclarationStmt) {}
    fn visit_dcl_init_list(&mut self, _: &'a dracaena::ast::declaration::DeclarationInitList) {}
    fn visit_dcl_comma_list(&mut self, _: &'a dracaena::ast::declaration::DeclarationCommaList) {}
    fn visit_dcl_init(&mut self, _: &'a dracaena::ast::declaration::DeclarationInit) {}
    fn visit_dcl(&mut self, _: &'a dracaena::ast::declaration::Declaration) {}
    fn visit_assign_clause(&mut self, _: &'a dracaena::ast::assignment::AssignmentClause) {}
    fn visit_assign_stmt(&mut self, _: &'a dracaena::ast::assignment::AssignmentStmt) {}
    fn visit_id_with_assign_clause(
        &mut self,
        _: &'a dracaena::ast::declaration::IdWithAssigmentClause,
    ) {
    }
    fn visit_assign(&mut self, _: &'a dracaena::ast::assignment::Assign) {}
    fn visit_pre_post_inc_dec_assign(
        &mut self,
        _: &'a dracaena::ast::assignment::PrePostIncDecAssign,
    ) {
    }
    fn visit_assign_inc_dec(&mut self, _: &'a dracaena::ast::assignment::AssignIncDec) {}
    fn visit_stmt(&mut self, _: &'a dracaena::ast::statement::Statement) {}
    fn visit_expr_stmt(&mut self, _: &'a dracaena::ast::statement::ExpressionStatement) {}
    fn visit_local_body(&mut self, _: &'a dracaena::ast::local_body::LocalBody) {}
}

impl<'a> VisitorEnterLeave<'a> for ConditionalExpressionContextVisitor<'a> {
    fn enter_visit_conditional_expr_data(&mut self, node: &'a ConditionalExpressionData) -> bool {
        self.current_cond_expr_data_level += 1;
        match (&*node.cond_expr_left, &node.log_op, &*node.cond_expr_right) {
            (
                ConditionalExpression::Expression(Expression::Comparison(ComparisonExpression {
                    simple_expr_left:
                        SimpleExpression::Term(Term::ComplementFactor(ComplementFactor::SecondFactor(
                            SecondFactor {
                                sign_op: None,
                                factor:
                                    Factor::Field(Field::QualifierId(
                                        Qualifier::Id(table_name_left),
                                        field_name_left,
                                    )),
                            },
                        ))),
                    comp_op: _comp_op_left,
                    simple_expr_right:
                        SimpleExpression::Term(Term::ComplementFactor(ComplementFactor::SecondFactor(
                            SecondFactor {
                                sign_op: None,
                                factor: Factor::Constant(_),
                            },
                        ))),
                })),
                LogicalOperator::Or,
                ConditionalExpression::Expression(Expression::Comparison(ComparisonExpression {
                    simple_expr_left:
                        SimpleExpression::Term(Term::ComplementFactor(ComplementFactor::SecondFactor(
                            SecondFactor {
                                sign_op: None,
                                factor:
                                    Factor::Field(Field::QualifierId(
                                        Qualifier::Id(table_name_right),
                                        field_name_right,
                                    )),
                            },
                        ))),
                    comp_op: _comp_op_right,
                    simple_expr_right:
                        SimpleExpression::Term(Term::ComplementFactor(ComplementFactor::SecondFactor(
                            SecondFactor {
                                sign_op: None,
                                factor: Factor::Constant(_),
                            },
                        ))),
                })),
            ) if table_name_left.eq_ignore_ascii_case(table_name_right.as_str())
                && !field_name_left.eq_ignore_ascii_case(field_name_right.as_str()) =>
            {
                self.cond_expr_ctx_resolve_strategy =
                    Some(ConditionalExpressionContextResolveStrategy::StrFmt);
                false
            }
            (
                ConditionalExpression::Expression(Expression::Comparison(ComparisonExpression {
                    simple_expr_left:
                        SimpleExpression::Term(Term::ComplementFactor(ComplementFactor::SecondFactor(
                            SecondFactor {
                                sign_op: None,
                                factor:
                                    Factor::Field(Field::QualifierId(
                                        Qualifier::Id(_table_name),
                                        _field_name,
                                    )),
                            },
                        ))),
                    comp_op: _,
                    simple_expr_right:
                        SimpleExpression::Term(Term::ComplementFactor(ComplementFactor::SecondFactor(
                            SecondFactor {
                                sign_op: None,
                                factor: Factor::Constant(_),
                            },
                        ))),
                })),
                _, //LogicalOperator::Or,
                _,
            )
            | (
                _,
                _, //LogicalOperator::Or,
                ConditionalExpression::Expression(Expression::Comparison(ComparisonExpression {
                    simple_expr_left:
                        SimpleExpression::Term(Term::ComplementFactor(ComplementFactor::SecondFactor(
                            SecondFactor {
                                sign_op: None,
                                factor:
                                    Factor::Field(Field::QualifierId(
                                        Qualifier::Id(_table_name),
                                        _field_name,
                                    )),
                            },
                        ))),
                    comp_op: _,
                    simple_expr_right:
                        SimpleExpression::Term(Term::ComplementFactor(ComplementFactor::SecondFactor(
                            SecondFactor {
                                sign_op: None,
                                factor: Factor::Constant(_),
                            },
                        ))),
                })),
            ) => {
                // TODO: we might need to do something here
                true
            }
            _ => {
                // TODO: handle the other cases
                true
            }
        }
    }

    fn leave_visit_conditional_expr_data(&mut self, _node: &'a ConditionalExpressionData) -> bool {
        self.current_cond_expr_data_level -= 1;
        if self.current_cond_expr_data_level == 0
            && self
                .cond_expr_ctx_resolve_strategy
                .as_ref()
                .map_or(true, |strategy| {
                    *strategy != ConditionalExpressionContextResolveStrategy::StrFmt
                })
        {
            let mut log_ops_parens_list: Vec<_> = (0..self.log_op_idx_to_log_op_parens_level.len())
                .fold(Vec::new(), |mut vec, idx| {
                    vec.push(self.log_op_idx_to_log_op_parens_level.get(&idx).unwrap());
                    vec
                });
            // we dedup, because something like the following is fine for `AddRange`:
            // where (custTable.AccountNum == 'Foo'
            //      || custTable.AccountNum == 'Bar')
            //      && (custTable.Name == 'Baz'
            //          || custTable.Name == 'Quux')
            //  --> && custTable.State == State::Active
            //  --> && custTable.Country == 'Germany';
            //
            // where (custTable.AccountNum == 'Foo'
            //  -->     || custTable.AccountNum == 'Bar'
            //  -->     || custTable.AccountNum == 'Baz')
            //      && (custTable.Name == 'Baz'
            //          || custTable.Name == 'Quux');
            log_ops_parens_list.dedup_by(|a, b| *a.log_op == *b.log_op);

            if log_ops_parens_list.len() > 2
                && log_ops_parens_list.windows(2).all(|window| match *window {
                    [LogOpParensLevel {
                        log_op: LogicalOperator::Or,
                        parenthesize_level: parenthesize_level_or,
                    }, LogOpParensLevel {
                        log_op: LogicalOperator::And,
                        parenthesize_level: parenthesize_level_and,
                    }]
                    | [LogOpParensLevel {
                        log_op: LogicalOperator::And,
                        parenthesize_level: parenthesize_level_and,
                    }, LogOpParensLevel {
                        log_op: LogicalOperator::Or,
                        parenthesize_level: parenthesize_level_or,
                    }] => parenthesize_level_or > parenthesize_level_and,
                    _ => false,
                })
            {
                self.cond_expr_ctx_resolve_strategy =
                    Some(ConditionalExpressionContextResolveStrategy::AddRange);
                false
            } else {
                let vec: Vec<_> = self.table_name_field_name_to_log_ops.iter().collect();
                if vec.windows(2).any(|slice| {
                    match slice {
                        [(_tf1, log_ops1), (_tf2, log_ops2)] => {
                            match (log_ops1.as_slice(), log_ops2.as_slice()) {
                                ([LogicalOperator::Or], [LogicalOperator::Or]) => true,
                                ([log_op1], [log_op2]) => log_op1 != log_op2,
                                (
                                    // the original conditional expression basically looks something like this:
                                    // where Foo.A == "Foo"
                                    //    || Foo.A == "Bar"
                                    //    && Foo.X == "Quux"
                                    // in this example Foo.A has two "||", because we go by *where || is bound to*,
                                    // which is the first Foo.A and the second Foo.A.
                                    // Another way to view this is that the first Foo.A has no left logical operator,
                                    // so it associates itself to the right operator and the second Foo.A has a left operator
                                    // so it associates itself to the same operator as the first Foo.A
                                    // => that's why we have two LogicalOperator::Or here
                                    [LogicalOperator::Or, LogicalOperator::Or],
                                    [LogicalOperator::And],
                                ) => false,
                                _ => true,
                            }
                        }
                        _ => unreachable!(),
                    }
                }) {
                    self.cond_expr_ctx_resolve_strategy =
                        Some(ConditionalExpressionContextResolveStrategy::StrFmt);
                    false
                } else {
                    self.cond_expr_ctx_resolve_strategy =
                        Some(ConditionalExpressionContextResolveStrategy::AddRange);
                    false
                }
            }
        } else {
            self.cond_expr_ctx_resolve_strategy
                .as_ref()
                .map_or(true, |strategy| {
                    *strategy != ConditionalExpressionContextResolveStrategy::StrFmt
                })
        }
    }

    fn enter_visit_conditional_expr(&mut self, node: &'a ConditionalExpression) -> bool {
        if node.parenthesized_inner().is_some() {
            self.current_parenthesize_level += 1;
        }
        true
    }

    fn leave_visit_conditional_expr(&mut self, node: &'a ConditionalExpression) -> bool {
        if node.parenthesized_inner().is_some() {
            self.current_parenthesize_level -= 1;
        }
        true
    }

    fn leave_visit_logical_op(&mut self, node: &'a LogicalOperator) -> bool {
        if self.current_log_op_idx > 0 {
            if let Some(log_op_parens) = self
                .log_op_idx_to_log_op_parens_level
                .get(&(self.current_log_op_idx - 1))
            {
                if (*log_op_parens.log_op == LogicalOperator::And
                    && *node == LogicalOperator::Or
                    && log_op_parens.parenthesize_level < self.current_parenthesize_level)
                    || (self.current_log_op_idx == 1
                        && *log_op_parens.log_op == LogicalOperator::Or
                        && *node == LogicalOperator::And
                        && log_op_parens.parenthesize_level > self.current_parenthesize_level)
                {
                    // this basically handles the following cases (`||` is in parentheses, whereas `&&` is outside of it)
                    // where (Foo.A == 'Foo' || Foo.A == 'Bar') && (Foo.B == 'Baz' || Foo.B == 'Quux)';
                    self.current_log_op_idx += 1;
                    return true;
                } else if *log_op_parens.log_op != *node
                    && log_op_parens.parenthesize_level < self.current_parenthesize_level
                {
                    // if somewhere in the conditional expression the natural priority
                    // of `&&` and `||` is altered through parentheses, we know it must
                    // resolve to `StrFmt` strategy
                    self.cond_expr_ctx_resolve_strategy =
                        Some(ConditionalExpressionContextResolveStrategy::StrFmt);
                    self.current_log_op_idx += 1;
                    return false;
                }
            }
        }
        self.current_log_op_idx += 1;
        true
    }

    fn enter_visit_comparison_expr(&mut self, node: &'a ComparisonExpression) -> bool {
        match (&node.simple_expr_left, &node.simple_expr_right) {
            (
                SimpleExpression::Term(Term::ComplementFactor(ComplementFactor::SecondFactor(
                    SecondFactor {
                        sign_op: None,
                        factor:
                            Factor::Field(Field::QualifierId(Qualifier::Id(table_name), field_name)),
                    },
                ))),
                _,
            )
            | (
                _,
                SimpleExpression::Term(Term::ComplementFactor(ComplementFactor::SecondFactor(
                    SecondFactor {
                        sign_op: None,
                        factor:
                            Factor::Field(Field::QualifierId(Qualifier::Id(table_name), field_name)),
                    },
                ))),
            ) => {
                if let Some(last_log_op) = self.last_log_op.as_ref() {
                    let log_ops = self
                        .table_name_field_name_to_log_ops
                        .entry(TableField::new(table_name, field_name))
                        .or_default();

                    log_ops.push(last_log_op);
                } else if self.current_cond_expr_data_level > 0 {
                    // this is our very first comparison expression within a conditional expression we encounter,
                    // so basically:
                    // foo.x && foo.y || ...
                    // ^^^^^
                    self.table_name_field_name_to_log_ops
                        .insert(TableField::new(table_name, field_name), Default::default());
                } else {
                    // we are not in a conditional expression, so we likely have something like the
                    // following simple case:
                    // where foo.x == "Foo";
                    self.cond_expr_ctx_resolve_strategy =
                        Some(ConditionalExpressionContextResolveStrategy::AddRange);
                    return false;
                }
                true
            }
            _ => {
                // TODO: implement other cases
                true
            }
        }
    }
}

#[derive(Default, Debug, PartialEq)]
pub struct ConditionalExpressionStrFmtContext {
    cond_expr_as_str_fmt: String,
    parm_elems_for_str_fmt: Vec<ParmElem>,
}

impl ConditionalExpressionStrFmtContext {
    pub fn new() -> Self {
        ConditionalExpressionStrFmtContext {
            ..Default::default()
        }
    }

    pub fn push_str_for_str_fmt(&mut self, str_ref: &str) -> &mut Self {
        self.cond_expr_as_str_fmt.push_str(str_ref);
        self
    }

    pub fn push_parm_elem_for_str_fmt(&mut self, parm_elem: ParmElem) -> &mut Self {
        match &parm_elem {
            ParmElem::TernaryExpression(tern_expr) => {
                let format_str = match &**tern_expr {
                    TernaryExpression::ConditionalExpression(
                        ConditionalExpression::Expression(Expression::Simple(
                            SimpleExpression::Term(Term::ComplementFactor(
                                ComplementFactor::SecondFactor(SecondFactor {
                                    sign_op: _,
                                    factor:
                                        Factor::Function(Function::QueryValue(
                                            ParmElem::TernaryExpression(tern_expr_inner),
                                        )),
                                }),
                            )),
                        )),
                    ) => match &**tern_expr_inner {
                        TernaryExpression::ConditionalExpression(
                            ConditionalExpression::Expression(Expression::Simple(
                                SimpleExpression::Term(Term::ComplementFactor(
                                    ComplementFactor::SecondFactor(SecondFactor {
                                        sign_op: _,
                                        factor,
                                    }),
                                )),
                            )),
                        ) => match factor {
                            Factor::Constant(Constant::String(_const_str)) => {
                                format!(r#""%{}""#, self.parm_elems_for_str_fmt.len() + 1)
                            }
                            _ => format!("%{}", self.parm_elems_for_str_fmt.len() + 1),
                        },
                        _ => todo!(),
                    },
                    _ => {
                        format!("%{}", self.parm_elems_for_str_fmt.len() + 1)
                    }
                };

                self.cond_expr_as_str_fmt.push_str(format_str.as_str());
                self.parm_elems_for_str_fmt.push(parm_elem);
            }
        };

        self
    }

    pub fn has_parm_elems_for_str_fmt(&self) -> bool {
        !self.parm_elems_for_str_fmt.is_empty()
    }

    pub fn pop_str_for_str_format(&mut self) -> Option<char> {
        self.cond_expr_as_str_fmt.pop()
    }

    pub fn into_function_str_fmt(self) -> Function {
        Function::StrFmt(
            iter::once(ParmElem::TernaryExpression(Box::new(
                Factor::Constant(Constant::String(if !self.cond_expr_as_str_fmt.is_empty() {
                    format!("({})", self.cond_expr_as_str_fmt)
                } else {
                    self.cond_expr_as_str_fmt
                }))
                .builder()
                .into(),
            )))
            .chain(self.parm_elems_for_str_fmt)
            .collect(),
        )
    }
}

#[cfg(test)]
mod tests {

    /// For these tests, test data is prepared by using the parser directly.
    /// This has the advantage that we don't need to prepare the ast structure
    /// manually, which saves a lot of complication in the tests.
    /// Of course this has the disadvantage that failing tests **could** have their origin
    /// not in this crate, but in the parser itself, which has bugs.
    /// However, this risk is tolerable and we live with it.
    mod conditional_expression_context_visitor {

        use super::super::*;

        use dracaena::visitor::visitable::*;
        use pretty_assertions::assert_eq;
        use selector::parsers::parser::SelectStmtParsed;

        #[test]
        fn cond_expr_single_simple_resolves_to_add_range() {
            let parse_result = r#"select custTable
                                    where custTable.AccountNum == 'Foo';"#
                .parse::<SelectStmtParsed>();

            assert!(
                parse_result.is_ok(),
                "Parsing failed: {}",
                parse_result.unwrap_err()
            );
            let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

            assert_eq!(left_over, "");

            let mut cond_expr_visitor = ConditionalExpressionContextVisitor::new();

            assert_eq!(cond_expr_visitor.cond_expr_ctx_resolve_strategy, None);

            ast.take_visitor_enter_leave(&mut cond_expr_visitor);

            assert_eq!(
                cond_expr_visitor.cond_expr_ctx_resolve_strategy,
                Some(ConditionalExpressionContextResolveStrategy::AddRange)
            );
        }

        #[test]
        fn cond_expr_single_simple_parenthesized_resolves_to_add_range() {
            let parse_result = r#"select custTable
                                    where (custTable.AccountNum == 'Foo');"#
                .parse::<SelectStmtParsed>();

            assert!(
                parse_result.is_ok(),
                "Parsing failed: {}",
                parse_result.unwrap_err()
            );
            let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

            assert_eq!(left_over, "");

            let mut cond_expr_visitor = ConditionalExpressionContextVisitor::new();

            assert_eq!(cond_expr_visitor.cond_expr_ctx_resolve_strategy, None);

            ast.take_visitor_enter_leave(&mut cond_expr_visitor);

            assert_eq!(
                cond_expr_visitor.cond_expr_ctx_resolve_strategy,
                Some(ConditionalExpressionContextResolveStrategy::AddRange)
            );
        }

        #[test]
        fn cond_expr_log_and_on_two_fields_resolves_to_add_range() {
            let parse_result = r#"select custTable
                                    where custTable.AccountNum == 'Foo'
                                        && custTable.Party == NoYes::Yes;"#
                .parse::<SelectStmtParsed>();

            assert!(
                parse_result.is_ok(),
                "Parsing failed: {}",
                parse_result.unwrap_err()
            );
            let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

            assert_eq!(left_over, "");

            let mut cond_expr_visitor = ConditionalExpressionContextVisitor::new();

            assert_eq!(cond_expr_visitor.cond_expr_ctx_resolve_strategy, None);

            ast.take_visitor_enter_leave(&mut cond_expr_visitor);

            assert_eq!(
                cond_expr_visitor.cond_expr_ctx_resolve_strategy,
                Some(ConditionalExpressionContextResolveStrategy::AddRange)
            );
        }

        #[test]
        fn cond_expr_log_and_on_two_fields_everything_parenthesized_resolves_to_add_range() {
            let parse_result = r#"select custTable
                                    where ((custTable.AccountNum == 'Foo')
                                        && (custTable.Party == NoYes::Yes));"#
                .parse::<SelectStmtParsed>();

            assert!(
                parse_result.is_ok(),
                "Parsing failed: {}",
                parse_result.unwrap_err()
            );
            let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

            assert_eq!(left_over, "");

            let mut cond_expr_visitor = ConditionalExpressionContextVisitor::new();

            assert_eq!(cond_expr_visitor.cond_expr_ctx_resolve_strategy, None);

            ast.take_visitor_enter_leave(&mut cond_expr_visitor);

            assert_eq!(
                cond_expr_visitor.cond_expr_ctx_resolve_strategy,
                Some(ConditionalExpressionContextResolveStrategy::AddRange)
            );
        }

        #[test]
        fn cond_expr_log_or_on_same_field_resolves_to_add_range() {
            let parse_result = r#"select custTable
                                    where custTable.AccountNum == 'Foo'
                                        || custTable.AccountNum == 'Bar';"#
                .parse::<SelectStmtParsed>();

            assert!(
                parse_result.is_ok(),
                "Parsing failed: {}",
                parse_result.unwrap_err()
            );
            let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

            assert_eq!(left_over, "");

            let mut cond_expr_visitor = ConditionalExpressionContextVisitor::new();

            assert_eq!(cond_expr_visitor.cond_expr_ctx_resolve_strategy, None);

            ast.take_visitor_enter_leave(&mut cond_expr_visitor);

            assert_eq!(
                cond_expr_visitor.cond_expr_ctx_resolve_strategy,
                Some(ConditionalExpressionContextResolveStrategy::AddRange)
            );
        }

        #[test]
        fn cond_expr_log_or_on_same_field_everything_parenthesized_resolves_to_add_range() {
            let parse_result = r#"select custTable
                                    where ((custTable.AccountNum == 'Foo')
                                        || (custTable.AccountNum == 'Bar'));"#
                .parse::<SelectStmtParsed>();

            assert!(
                parse_result.is_ok(),
                "Parsing failed: {}",
                parse_result.unwrap_err()
            );
            let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

            assert_eq!(left_over, "");

            let mut cond_expr_visitor = ConditionalExpressionContextVisitor::new();

            assert_eq!(cond_expr_visitor.cond_expr_ctx_resolve_strategy, None);

            ast.take_visitor_enter_leave(&mut cond_expr_visitor);

            assert_eq!(
                cond_expr_visitor.cond_expr_ctx_resolve_strategy,
                Some(ConditionalExpressionContextResolveStrategy::AddRange)
            );
        }

        #[test]
        fn cond_expr_log_or_on_two_fields_resolves_to_strfmt() {
            let parse_result = r#"select custTable
                                    where custTable.AccountNum == 'Foo'
                                        || custTable.Active == NoYes::Yes;"#
                .parse::<SelectStmtParsed>();

            assert!(
                parse_result.is_ok(),
                "Parsing failed: {}",
                parse_result.unwrap_err()
            );
            let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

            assert_eq!(left_over, "");

            let mut cond_expr_visitor = ConditionalExpressionContextVisitor::new();

            assert_eq!(cond_expr_visitor.cond_expr_ctx_resolve_strategy, None);

            ast.take_visitor_enter_leave(&mut cond_expr_visitor);

            assert_eq!(
                cond_expr_visitor.cond_expr_ctx_resolve_strategy,
                Some(ConditionalExpressionContextResolveStrategy::StrFmt)
            );
        }

        #[test]
        fn cond_expr_log_or_on_two_fields_everything_parenthesized_resolves_to_strfmt() {
            let parse_result = r#"select custTable
                                    where ((custTable.AccountNum == 'Foo')
                                        || (custTable.Active == NoYes::Yes));"#
                .parse::<SelectStmtParsed>();

            assert!(
                parse_result.is_ok(),
                "Parsing failed: {}",
                parse_result.unwrap_err()
            );
            let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

            assert_eq!(left_over, "");

            let mut cond_expr_visitor = ConditionalExpressionContextVisitor::new();

            assert_eq!(cond_expr_visitor.cond_expr_ctx_resolve_strategy, None);

            ast.take_visitor_enter_leave(&mut cond_expr_visitor);

            assert_eq!(
                cond_expr_visitor.cond_expr_ctx_resolve_strategy,
                Some(ConditionalExpressionContextResolveStrategy::StrFmt)
            );
        }

        #[test]
        fn cond_expr_log_or_on_same_field_with_cond_expr_log_and_on_different_field_natural_sequence_resolves_to_add_range(
        ) {
            let parse_result = r#"select custTable
                                    where custTable.AccountNum == 'Foo'
                                        || custTable.AccountNum == 'Bar'
                                        && custTable.IsActive == NoYes::Yes;"#
                .parse::<SelectStmtParsed>();

            assert!(
                parse_result.is_ok(),
                "Parsing failed: {}",
                parse_result.unwrap_err()
            );
            let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

            assert_eq!(left_over, "");

            let mut cond_expr_visitor = ConditionalExpressionContextVisitor::new();

            assert_eq!(cond_expr_visitor.cond_expr_ctx_resolve_strategy, None);

            ast.take_visitor_enter_leave(&mut cond_expr_visitor);

            assert_eq!(
                cond_expr_visitor.cond_expr_ctx_resolve_strategy,
                Some(ConditionalExpressionContextResolveStrategy::AddRange)
            );
        }

        #[test]
        fn cond_expr_log_and_on_different_field_with_cond_expr_log_or_on_same_field_natural_sequence_resolves_to_strfmt(
        ) {
            let parse_result = r#"select custTable
                                    where custTable.AccountNum == 'Foo'
                                        && custTable.Name == 'Bar'
                                        || custTable.Name == 'Quux';"#
                .parse::<SelectStmtParsed>();

            assert!(
                parse_result.is_ok(),
                "Parsing failed: {}",
                parse_result.unwrap_err()
            );
            let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

            assert_eq!(left_over, "");

            let mut cond_expr_visitor = ConditionalExpressionContextVisitor::new();

            assert_eq!(cond_expr_visitor.cond_expr_ctx_resolve_strategy, None);

            ast.take_visitor_enter_leave(&mut cond_expr_visitor);

            assert_eq!(
                cond_expr_visitor.cond_expr_ctx_resolve_strategy,
                Some(ConditionalExpressionContextResolveStrategy::StrFmt)
            );
        }

        #[test]
        fn cond_expr_log_and_on_different_field_with_cond_expr_log_or_on_same_field_same_field_is_at_beginning_and_end_natural_sequence_resolves_to_strfmt(
        ) {
            let parse_result = r#"select custTable
                                    where custTable.Name == 'Quux'
                                        && custTable.AccountNum == 'Foo'
                                        || custTable.Name == 'Bar';"#
                .parse::<SelectStmtParsed>();

            assert!(
                parse_result.is_ok(),
                "Parsing failed: {}",
                parse_result.unwrap_err()
            );
            let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

            assert_eq!(left_over, "");

            let mut cond_expr_visitor = ConditionalExpressionContextVisitor::new();

            assert_eq!(cond_expr_visitor.cond_expr_ctx_resolve_strategy, None);

            ast.take_visitor_enter_leave(&mut cond_expr_visitor);

            assert_eq!(
                cond_expr_visitor.cond_expr_ctx_resolve_strategy,
                Some(ConditionalExpressionContextResolveStrategy::StrFmt)
            );
        }

        #[test]
        fn cond_expr_log_or_on_same_field_parenthesized_with_log_and_on_different_field_natural_sequence_resolves_to_add_range(
        ) {
            let parse_result = r#"select custTable
                                    where (custTable.AccountNum == 'Foo'
                                            || custTable.AccountNum == 'Bar')
                                        && custTable.IsActive == NoYes::Yes;"#
                .parse::<SelectStmtParsed>();

            assert!(
                parse_result.is_ok(),
                "Parsing failed: {}",
                parse_result.unwrap_err()
            );
            let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

            assert_eq!(left_over, "");

            let mut cond_expr_visitor = ConditionalExpressionContextVisitor::new();

            assert_eq!(cond_expr_visitor.cond_expr_ctx_resolve_strategy, None);

            ast.take_visitor_enter_leave(&mut cond_expr_visitor);

            assert_eq!(
                cond_expr_visitor.cond_expr_ctx_resolve_strategy,
                Some(ConditionalExpressionContextResolveStrategy::AddRange)
            );
        }

        #[test]
        fn cond_expr_log_or_on_same_field_with_cond_expr_log_and_on_different_field_and_parenthesized_resolves_to_strfmt(
        ) {
            let parse_result = r#"select custTable
                                    where custTable.AccountNum == 'Foo'
                                        || (custTable.AccountNum == 'Bar'
                                            && custTable.IsActive == NoYes::Yes);"#
                .parse::<SelectStmtParsed>();

            assert!(
                parse_result.is_ok(),
                "Parsing failed: {}",
                parse_result.unwrap_err()
            );
            let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

            assert_eq!(left_over, "");

            let mut cond_expr_visitor = ConditionalExpressionContextVisitor::new();

            assert_eq!(cond_expr_visitor.cond_expr_ctx_resolve_strategy, None);

            ast.take_visitor_enter_leave(&mut cond_expr_visitor);

            assert_eq!(
                cond_expr_visitor.cond_expr_ctx_resolve_strategy,
                Some(ConditionalExpressionContextResolveStrategy::StrFmt)
            );
        }

        #[test]
        fn cond_expr_log_or_on_different_field_with_cond_expr_log_and_on_different_field_natural_sequence_resolves_to_strfmt(
        ) {
            let parse_result = r#"select custTable
                                    where custTable.AccountNum == 'Foo'
                                        || custTable.State == State::Happy
                                        && custTable.IsActive == NoYes::Yes;"#
                .parse::<SelectStmtParsed>();

            assert!(
                parse_result.is_ok(),
                "Parsing failed: {}",
                parse_result.unwrap_err()
            );
            let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

            assert_eq!(left_over, "");

            let mut cond_expr_visitor = ConditionalExpressionContextVisitor::new();

            assert_eq!(cond_expr_visitor.cond_expr_ctx_resolve_strategy, None);

            ast.take_visitor_enter_leave(&mut cond_expr_visitor);

            assert_eq!(
                cond_expr_visitor.cond_expr_ctx_resolve_strategy,
                Some(ConditionalExpressionContextResolveStrategy::StrFmt)
            );
        }

        #[test]
        fn cond_expr_log_or_on_different_field_with_cond_expr_log_and_on_different_field_and_parenthesized_resolves_to_strfmt(
        ) {
            let parse_result = r#"select custTable
                                    where custTable.AccountNum == 'Foo'
                                        || (custTable.State == State::Happy
                                            && custTable.IsActive == NoYes::Yes);"#
                .parse::<SelectStmtParsed>();

            assert!(
                parse_result.is_ok(),
                "Parsing failed: {}",
                parse_result.unwrap_err()
            );
            let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

            assert_eq!(left_over, "");

            let mut cond_expr_visitor = ConditionalExpressionContextVisitor::new();

            assert_eq!(cond_expr_visitor.cond_expr_ctx_resolve_strategy, None);

            ast.take_visitor_enter_leave(&mut cond_expr_visitor);

            assert_eq!(
                cond_expr_visitor.cond_expr_ctx_resolve_strategy,
                Some(ConditionalExpressionContextResolveStrategy::StrFmt)
            );
        }

        #[test]
        fn cond_expr_log_or_with_cond_expr_log_or_parenthesized_with_same_fields_resolves_to_strfmt(
        ) {
            let parse_result = r#"select custTable
                                    where custTable.AccountNum == 'Foo'
                                        || (custTable.State == State::Happy
                                            || custTable.State == State::Tired);"#
                .parse::<SelectStmtParsed>();

            assert!(
                parse_result.is_ok(),
                "Parsing failed: {}",
                parse_result.unwrap_err()
            );
            let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

            assert_eq!(left_over, "");

            let mut cond_expr_visitor = ConditionalExpressionContextVisitor::new();

            assert_eq!(cond_expr_visitor.cond_expr_ctx_resolve_strategy, None);

            ast.take_visitor_enter_leave(&mut cond_expr_visitor);

            assert_eq!(
                cond_expr_visitor.cond_expr_ctx_resolve_strategy,
                Some(ConditionalExpressionContextResolveStrategy::StrFmt)
            );
        }

        #[test]
        fn cond_expr_log_or_parenthesized_with_different_fields_with_cond_expr_log_or_on_same_fields_natural_sequence_resolves_to_strfmt(
        ) {
            let parse_result = r#"select custTable
                                    where (custTable.AccountNum == 'Foo'
                                            || custTable.State == State::Happy)
                                        || custTable.State == State::Tired;"#
                .parse::<SelectStmtParsed>();

            assert!(
                parse_result.is_ok(),
                "Parsing failed: {}",
                parse_result.unwrap_err()
            );
            let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

            assert_eq!(left_over, "");

            let mut cond_expr_visitor = ConditionalExpressionContextVisitor::new();

            assert_eq!(cond_expr_visitor.cond_expr_ctx_resolve_strategy, None);

            ast.take_visitor_enter_leave(&mut cond_expr_visitor);

            assert_eq!(
                cond_expr_visitor.cond_expr_ctx_resolve_strategy,
                Some(ConditionalExpressionContextResolveStrategy::StrFmt)
            );
        }

        #[test]
        fn cond_expr_log_or_with_cond_expr_log_or_parenthesized_same_field_with_cond_expr_log_and_parenthesized_resolves_to_strfmt(
        ) {
            let parse_result = r#"select custTable
                                    where custTable.AccountNum == 'Foo'
                                        || ((custTable.State == State::Happy
                                            && custTable.IsActive == NoYes::Yes)
                                            || custTable.AccountNum == 'Bar');"#
                .parse::<SelectStmtParsed>();

            assert!(
                parse_result.is_ok(),
                "Parsing failed: {}",
                parse_result.unwrap_err()
            );
            let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

            assert_eq!(left_over, "");

            let mut cond_expr_visitor = ConditionalExpressionContextVisitor::new();

            assert_eq!(cond_expr_visitor.cond_expr_ctx_resolve_strategy, None);

            ast.take_visitor_enter_leave(&mut cond_expr_visitor);

            assert_eq!(
                cond_expr_visitor.cond_expr_ctx_resolve_strategy,
                Some(ConditionalExpressionContextResolveStrategy::StrFmt)
            );
        }

        #[test]
        fn cond_expr_log_or_same_field_parenthesized_log_and_cond_expr_log_or_same_field_parenthesized_resolves_to_add_range(
        ) {
            let parse_result = r#"select custTable
                                    where (custTable.AccountNum == 'Foo'
                                            || custTable.AccountNum == 'Bar')
                                        && (custTable.Name == 'Baz'
                                            || custTable.Name == 'Quux');"#
                .parse::<SelectStmtParsed>();

            assert!(
                parse_result.is_ok(),
                "Parsing failed: {}",
                parse_result.unwrap_err()
            );
            let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

            assert_eq!(left_over, "");

            let mut cond_expr_visitor = ConditionalExpressionContextVisitor::new();

            assert_eq!(cond_expr_visitor.cond_expr_ctx_resolve_strategy, None);

            ast.take_visitor_enter_leave(&mut cond_expr_visitor);

            assert_eq!(
                cond_expr_visitor.cond_expr_ctx_resolve_strategy,
                Some(ConditionalExpressionContextResolveStrategy::AddRange)
            );
        }

        #[test]
        fn cond_expr_log_or_same_field_parenthesized_log_and_cond_expr_log_or_same_field_parenthesized_cond_expr_log_and_different_field_resolves_to_add_range(
        ) {
            let parse_result = r#"select custTable
                                    where (custTable.AccountNum == 'Foo'
                                            || custTable.AccountNum == 'Bar')
                                        && (custTable.Name == 'Baz'
                                            || custTable.Name == 'Quux')
                                        && custTable.State == State::Active;"#
                .parse::<SelectStmtParsed>();

            assert!(
                parse_result.is_ok(),
                "Parsing failed: {}",
                parse_result.unwrap_err()
            );
            let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

            assert_eq!(left_over, "");

            let mut cond_expr_visitor = ConditionalExpressionContextVisitor::new();

            assert_eq!(cond_expr_visitor.cond_expr_ctx_resolve_strategy, None);

            ast.take_visitor_enter_leave(&mut cond_expr_visitor);

            assert_eq!(
                cond_expr_visitor.cond_expr_ctx_resolve_strategy,
                Some(ConditionalExpressionContextResolveStrategy::AddRange)
            );
        }

        #[test]
        fn cond_expr_log_or_same_field_parenthesized_log_and_cond_expr_log_or_same_field_parenthesized_cond_expr_log_and_multiple_different_field_resolves_to_add_range(
        ) {
            let parse_result = r#"select custTable
                                    where (custTable.AccountNum == 'Foo'
                                            || custTable.AccountNum == 'Bar')
                                        && (custTable.Name == 'Baz'
                                            || custTable.Name == 'Quux')
                                        && custTable.State == State::Active
                                        && custTable.Country == 'Germany';"#
                .parse::<SelectStmtParsed>();

            assert!(
                parse_result.is_ok(),
                "Parsing failed: {}",
                parse_result.unwrap_err()
            );
            let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

            assert_eq!(left_over, "");

            let mut cond_expr_visitor = ConditionalExpressionContextVisitor::new();

            assert_eq!(cond_expr_visitor.cond_expr_ctx_resolve_strategy, None);

            ast.take_visitor_enter_leave(&mut cond_expr_visitor);

            assert_eq!(
                cond_expr_visitor.cond_expr_ctx_resolve_strategy,
                Some(ConditionalExpressionContextResolveStrategy::AddRange)
            );
        }

        #[test]
        fn cond_expr_log_or_multiple_same_field_parenthesized_log_and_cond_expr_log_or_same_field_parenthesized_resolves_to_add_range(
        ) {
            let parse_result = r#"select custTable
                                    where (custTable.AccountNum == 'Foo'
                                            || custTable.AccountNum == 'Bar'
                                            || custTable.AccountNum == 'Baz')
                                        && (custTable.Name == 'Baz'
                                            || custTable.Name == 'Quux');"#
                .parse::<SelectStmtParsed>();

            assert!(
                parse_result.is_ok(),
                "Parsing failed: {}",
                parse_result.unwrap_err()
            );
            let SelectStmtParsed { ast, left_over } = parse_result.unwrap();

            assert_eq!(left_over, "");

            let mut cond_expr_visitor = ConditionalExpressionContextVisitor::new();

            assert_eq!(cond_expr_visitor.cond_expr_ctx_resolve_strategy, None);

            ast.take_visitor_enter_leave(&mut cond_expr_visitor);

            assert_eq!(
                cond_expr_visitor.cond_expr_ctx_resolve_strategy,
                Some(ConditionalExpressionContextResolveStrategy::AddRange)
            );
        }
    }

    mod conditional_expression_strfmt_context {

        use super::super::*;
        use dracaena::ast::field::Field;
        use pretty_assertions::assert_eq;

        #[test]
        fn push_nothing_strfmt_has_one_param_str_is_empty() {
            let cond_expr_ctx = ConditionalExpressionStrFmtContext::new();

            let str_fmt_actual = cond_expr_ctx.into_function_str_fmt();
            let str_fmt_expected = Function::StrFmt(vec![ParmElem::TernaryExpression(Box::new(
                Factor::Constant(Constant::String(String::new()))
                    .builder()
                    .into(),
            ))]);

            assert_eq!(str_fmt_actual, str_fmt_expected);
        }

        #[test]
        fn push_one_str_strfmt_has_one_param_str_is_parenthesized() {
            let mut cond_expr_ctx = ConditionalExpressionStrFmtContext::new();
            cond_expr_ctx.push_str_for_str_fmt("Foo");

            let str_fmt_actual = cond_expr_ctx.into_function_str_fmt();
            let str_fmt_expected = Function::StrFmt(vec![ParmElem::TernaryExpression(Box::new(
                Factor::Constant(Constant::String("(Foo)".into()))
                    .builder()
                    .into(),
            ))]);

            assert_eq!(str_fmt_actual, str_fmt_expected);
        }

        #[test]
        fn push_one_parm_elem_strfmt_has_two_params_elem_is_percent_encoded_and_parenthesized() {
            let mut cond_expr_ctx = ConditionalExpressionStrFmtContext::new();
            cond_expr_ctx.push_parm_elem_for_str_fmt(ParmElem::TernaryExpression(Box::new(
                Factor::Field(Field::Id("AccountNum".into()))
                    .builder()
                    .into(),
            )));

            let str_fmt_actual = cond_expr_ctx.into_function_str_fmt();
            let str_fmt_expected = Function::StrFmt(vec![
                ParmElem::TernaryExpression(Box::new(
                    Factor::Constant(Constant::String("(%1)".into()))
                        .builder()
                        .into(),
                )),
                ParmElem::TernaryExpression(Box::new(
                    Factor::Field(Field::Id("AccountNum".into()))
                        .builder()
                        .into(),
                )),
            ]);

            assert_eq!(str_fmt_actual, str_fmt_expected);
        }

        #[test]
        fn push_one_str_and_one_parm_elem_strfmt_has_two_params_elem_is_percent_encoded_and_both_are_parenthesized(
        ) {
            let mut cond_expr_ctx = ConditionalExpressionStrFmtContext::new();
            cond_expr_ctx
                .push_str_for_str_fmt("x")
                .push_parm_elem_for_str_fmt(ParmElem::TernaryExpression(Box::new(
                    Factor::Field(Field::Id("AccountNum".into()))
                        .builder()
                        .into(),
                )));

            let str_fmt_actual = cond_expr_ctx.into_function_str_fmt();
            let str_fmt_expected = Function::StrFmt(vec![
                ParmElem::TernaryExpression(Box::new(
                    Factor::Constant(Constant::String("(x%1)".into()))
                        .builder()
                        .into(),
                )),
                ParmElem::TernaryExpression(Box::new(
                    Factor::Field(Field::Id("AccountNum".into()))
                        .builder()
                        .into(),
                )),
            ]);

            assert_eq!(str_fmt_actual, str_fmt_expected);
        }

        #[test]
        fn push_two_strs_and_one_parm_elem_strfmt_has_two_params_elem_is_percent_encoded() {
            let mut cond_expr_ctx = ConditionalExpressionStrFmtContext::new();
            cond_expr_ctx
                .push_str_for_str_fmt("x")
                .push_str_for_str_fmt("y")
                .push_parm_elem_for_str_fmt(ParmElem::TernaryExpression(Box::new(
                    Factor::Field(Field::Id("AccountNum".into()))
                        .builder()
                        .into(),
                )));

            let str_fmt_actual = cond_expr_ctx.into_function_str_fmt();
            let str_fmt_expected = Function::StrFmt(vec![
                ParmElem::TernaryExpression(Box::new(
                    Factor::Constant(Constant::String("(xy%1)".into()))
                        .builder()
                        .into(),
                )),
                ParmElem::TernaryExpression(Box::new(
                    Factor::Field(Field::Id("AccountNum".into()))
                        .builder()
                        .into(),
                )),
            ]);

            assert_eq!(str_fmt_actual, str_fmt_expected);
        }

        #[test]
        fn push_str_one_parm_elem_and_again_str_strfmt_has_two_params_elem_is_percent_encoded() {
            let mut cond_expr_ctx = ConditionalExpressionStrFmtContext::new();
            cond_expr_ctx
                .push_str_for_str_fmt("x")
                .push_parm_elem_for_str_fmt(ParmElem::TernaryExpression(Box::new(
                    Factor::Field(Field::Id("AccountNum".into()))
                        .builder()
                        .into(),
                )))
                .push_str_for_str_fmt("y");

            let str_fmt_actual = cond_expr_ctx.into_function_str_fmt();
            let str_fmt_expected = Function::StrFmt(vec![
                ParmElem::TernaryExpression(Box::new(
                    Factor::Constant(Constant::String("(x%1y)".into()))
                        .builder()
                        .into(),
                )),
                ParmElem::TernaryExpression(Box::new(
                    Factor::Field(Field::Id("AccountNum".into()))
                        .builder()
                        .into(),
                )),
            ]);

            assert_eq!(str_fmt_actual, str_fmt_expected);
        }

        #[test]
        fn push_two_parm_elems_strfmt_has_three_params_both_elems_are_percent_encoded() {
            let mut cond_expr_ctx = ConditionalExpressionStrFmtContext::new();
            cond_expr_ctx
                .push_parm_elem_for_str_fmt(ParmElem::TernaryExpression(Box::new(
                    Factor::Field(Field::Id("AccountNum".into()))
                        .builder()
                        .into(),
                )))
                .push_parm_elem_for_str_fmt(ParmElem::TernaryExpression(Box::new(
                    Factor::Field(Field::Id("Foo".into())).builder().into(),
                )));

            let str_fmt_actual = cond_expr_ctx.into_function_str_fmt();
            let str_fmt_expected = Function::StrFmt(vec![
                ParmElem::TernaryExpression(Box::new(
                    Factor::Constant(Constant::String("(%1%2)".into()))
                        .builder()
                        .into(),
                )),
                ParmElem::TernaryExpression(Box::new(
                    Factor::Field(Field::Id("AccountNum".into()))
                        .builder()
                        .into(),
                )),
                ParmElem::TernaryExpression(Box::new(
                    Factor::Field(Field::Id("Foo".into())).builder().into(),
                )),
            ]);

            assert_eq!(str_fmt_actual, str_fmt_expected);
        }

        #[test]
        fn push_str_and_parm_elems_consecutively_strfmt_has_three_params_both_elems_are_percent_encoded(
        ) {
            let mut cond_expr_ctx = ConditionalExpressionStrFmtContext::new();
            cond_expr_ctx
                .push_str_for_str_fmt("x")
                .push_parm_elem_for_str_fmt(ParmElem::TernaryExpression(Box::new(
                    Factor::Field(Field::Id("AccountNum".into()))
                        .builder()
                        .into(),
                )))
                .push_str_for_str_fmt("y")
                .push_parm_elem_for_str_fmt(ParmElem::TernaryExpression(Box::new(
                    Factor::Field(Field::Id("Foo".into())).builder().into(),
                )));

            let str_fmt_actual = cond_expr_ctx.into_function_str_fmt();
            let str_fmt_expected = Function::StrFmt(vec![
                ParmElem::TernaryExpression(Box::new(
                    Factor::Constant(Constant::String("(x%1y%2)".into()))
                        .builder()
                        .into(),
                )),
                ParmElem::TernaryExpression(Box::new(
                    Factor::Field(Field::Id("AccountNum".into()))
                        .builder()
                        .into(),
                )),
                ParmElem::TernaryExpression(Box::new(
                    Factor::Field(Field::Id("Foo".into())).builder().into(),
                )),
            ]);

            assert_eq!(str_fmt_actual, str_fmt_expected);
        }
    }
}
